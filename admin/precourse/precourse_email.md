Dear 2024 MIC Course Participant,

We are looking forward to the beginning of the 2024 MIC Course and we hope you are too.  We realize that this email message is long, but ask that you read the whole thing carefully because it contains many important details.  

-------------------------------------------- 
TODO Items
-------------------------------------------- 
You should have already received a separate email through Duke's Qualtrics with information on how to access the course computing environment.  Please follow the instructions in that email to confirm that you can log into the system, if you have not done so already. It is important to so this as soon as possible so that we can identify and resolve any problems in advance. Delays in identifying and resolving issues might prevent you from participating in computational sessions. If you haven't received the email, please check your junk folder and contact us <miccourse@duke.edu> if you can't find it.

You will receive another separate email from Dr. Jennifer Hill, the MIC Course evaluator, asking you to fill out a pre-course assessment. Once you recieve this email.  Please fill it out in a timely manner to help our evaluation and improvement of the course.

-------------------------------------------- 
Times
-------------------------------------------- 
The course will begin on Monday May 13th at 9am (EDT). The course sessions will be held 9am - 4pm (EDT)

-------------------------------------------- 
Computer
-------------------------------------------- 
Any computer that can comfortably run a current version of a modern web browser should be fine.  We will be working in a web-based computing environment so all the computational “heavy lifting" happens on the Duke Compute Cluster.  

-------------------------------------------- 
Location
-------------------------------------------- 
The course will be entirely on Zoom. Details for the course Zoom meeting are at the bottom of this email..


Sincerely,

The MIC Course Staff and Instructors


XXXXXXXX ADD ZOOM INFO BEFORE SENDING XXXXXXXX
XXXXXXXX ADD ZOOM INFO BEFORE SENDING XXXXXXXX
XXXXXXXX ADD ZOOM INFO BEFORE SENDING XXXXXXXX
XXXXXXXX ADD ZOOM INFO BEFORE SENDING XXXXXXXX
XXXXXXXX ADD ZOOM INFO BEFORE SENDING XXXXXXXX
XXXXXXXX ADD ZOOM INFO BEFORE SENDING XXXXXXXX
XXXXXXXX ADD ZOOM INFO BEFORE SENDING XXXXXXXX
XXXXXXXX ADD ZOOM INFO BEFORE SENDING XXXXXXXX
XXXXXXXX ADD ZOOM INFO BEFORE SENDING XXXXXXXX
