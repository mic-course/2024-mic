All computational work for the 2024 MIC course will be done through a web browser based interface to the Duke Compute Cluster. The only software you will  need on your computer is an up-to-date version of a modern web browser. 

However, we need you to do a login confirmation for your MIC course computing environment as soon as possible to be sure that everything is ready for the first day of class.  Please complete the following steps to confirmation your MIC course computing environment is working as expected.

1. Log into the computing environment using [these instructions](https://gitlab.oit.duke.edu/mic-course/2024-mic/-/blob/main/admin/ondemand_howto.md)

2. Once you are successfully logged in, your webrowser should show RStudio, which should look something like [this](../images/rstudio_browser.png)

3. Click in the **Console** pane (left side)

4. Type the following in the **Console** (you can also copy-paste): 
`file.show("/work/mic2024/confirm")`

5. If the above worked, you should get a message with a confirmation number.

6. Enter the confirmation number into the Qualtrics survey that you were sent separately. Once you do that you should receive a message "Success! Thank you for confirming you MIC Course computing environment login!" If you have a problem with this, please email <miccourse@duke.edu>
