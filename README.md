Shortened URL: https://duke.is/mic2024

# Schedule
- [Week 1 Schedule](admin/week1_schedule.md)
- [Week 2 Schedule](admin/week2_schedule.md)

# Computing Environments
- [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/)
- [RStudio on DCC OnDemand](content/computing/dcc_ood/dcc_ood_rstudio.md)
- [DCC Desktop](../content/computing/dcc_ood/dcc_ood_desktop.md)

# Workshop Content
- [Initial download of workshop content](content/computingreproducible/git_cloning.md)
- [Update workshop content](content/computing/reproducible/git_pull.md)

# Miscellaneous
- [Managing Notes](admin/managing_file_modifications.md)
- [After the MIC Course](admin/post_course_resources.md)

# Course Dataset
We are using data from [Clonal architecture in mesothelioma is prognostic and shapes the tumour microenvironment](https://pubmed.ncbi.nlm.nih.gov/33741915/). The raw sequence data from this paper can be downloaded from NCBI [BioProject PRJNA649889](https://www.ncbi.nlm.nih.gov/bioproject/?term=PRJNA649889).