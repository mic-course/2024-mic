
```{r}
library(tidyverse)
```

Please use the built-in `mpg` data set for these questions.

```{r}
?mpg
head(mpg)
```


1. Create a scatter plot of `cty` (city miles per gallon) vs. `hwy` (highway miles per gallon) from the `mpg` data set. 

```{r}
ggplot(data = mpg, aes(x = cty, y = hwy)) +
  geom_point()
```

2. Add a regression line to the plot.

```{r}
ggplot(data = mpg, aes(x = cty, y = hwy)) +
  geom_point() +
  geom_smooth(method = "lm", formula = y ~ x)
```

3. Color the dots by `drv` (type of drive train), and make sure that there is a regression line for each `drv` type.

```{r}
ggplot(data = mpg, aes(x = cty, y = hwy, color = drv)) +
  geom_point() +
  geom_smooth(method = "lm", formula = y ~ x)
```

4. For each type of drive train `drv`, create a box plot of `displ` (engine displacement) by `fl` (fuel type), overlay points on top of the box plot, color the points by `class` (type of car). Arrange these box plots side by side.

```{r}
ggplot(data = mpg, aes(x = fl, y = displ)) +
  geom_boxplot() + 
  geom_point(aes(color = class)) +
  facet_wrap(~ drv)
```

5. Add proper axis/legend titles to this plot.

```{r}
ggplot(data = mpg, aes(x = fl, y = displ)) +
  geom_boxplot() + 
  geom_point(aes(color = class)) +
  facet_wrap(~ drv) +
  labs(x = "Fuel Type", y = "Engine Displacement", color = "Type of Car")
```

6. Make the axis/legend titles bold and set their font sizes to 11 pt.

```{r}
ggplot(data = mpg, aes(x = fl, y = displ)) +
  geom_boxplot() + 
  geom_point(aes(color = class)) +
  facet_wrap(~ drv) +
  labs(x = "Fuel Type", y = "Engine Displacement", color = "Type of Car") +
  theme(axis.title = element_text(size = 11, face = "bold"),
        legend.title = element_text(size = 11, face = "bold"))
```

7. Save this box plot to a file called "mpg_boxplot.png", and display it in the markdown file.

```{r}
p <- ggplot(data = mpg, aes(x = fl, y = displ)) +
  geom_boxplot() + 
  geom_point(aes(color = class)) +
  facet_wrap(~ drv) +
  labs(x = "Fuel Type", y = "Engine Displacement", color = "Type of Car") +
  theme(axis.title = element_text(size = 11, face = "bold"),
        legend.title = element_text(size = 11, face = "bold"))

ggsave(p, file = "./mpg_boxplot.png")
```

![](./mpg_boxplot.png)
