
# Starting up a Linux Desktop on DCC OnDemand
1. Go to [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/)
2. Sign in with your NetID and password
3. Click on **Interactive Apps** at the top and select **DCC Desktop** 
    - If you don't see **Interactive Apps** and only see three lines in the top right, click on the three lines, then **Interactive Apps**, then **DCC Desktop**
4. A new page should open that says **DCC Desktop** at the top.
5. Make sure that the settings match this:
    - *Account:* mic-2024
    - *Number of hours:* 8
    - *Partition:* chsi-high
    - *Memory requested (Gb):* 20
    - *CPUs per task:* 2
    - *GPUs:* 0
6. Scroll to the bottom and click **Launch**
7. A new page should open with a box that says **DCC Desktop**, you may see the following messages:
    - "Please be patient as your job currently sits in queue. The wait time depends on the number of cores as well as time requested."
    - "Your session is currently starting... Please be patient as this process can take a few minutes."
8. You are waiting for a blue button to appear that says **Launch DCC Desktop**. This could take a minute or so. When it appears, click on it.
9. After a minute or so, an AlmaLinux Desktop should open in your webbrowser.

# Shutting Down
1. In the AlmaLinux Desktop click on **Applications** in the top left corner, then select **Log Out**
2. A box should pop up that says **Log out [your name]**; click on the **Log Out** button in this box.
3. Now the screen should switch to say **noVNC** with a **Connect** button, once this happens you can close the browser window or the browser tab.
4. You are done!
