srun \
  --partition=chsi-high \
  --account=mic-2024 \
  --nodes=1 \
  --ntasks-per-node=1 \
  --cpus-per-task=20 \
  --pty bash -i

wd=/work/mic-2024-gatk/annotation/vepcache
sif=/opt/apps/containers/community/mic-2024/dcibioinformaticsVEP-v2.0.sif

singularity exec \
  --containall \
  --no-home \
  --bind ${wd}:${wd}:rw \
  ${sif} \
  /opt/vep/src/ensembl-vep/INSTALL.pl \
  --CACHEDIR ${wd} \
  --AUTO cf \
  --SPECIES homo_sapiens \
  --ASSEMBLY GRCh38
#  - getting list of available cache files
#  - downloading https://ftp.ensembl.org/pub/release-111/variation/indexed_vep_cache/homo_sapiens_vep_111_GRCh38.tar.gz
# ERROR: checksum for /work/mic-2024-gatk/annotation/vepcache/tmp/homo_sapiens_vep_111_GRCh38.tar.gz doesn't 
# match checksum in CHECKSUMS file on FTP site