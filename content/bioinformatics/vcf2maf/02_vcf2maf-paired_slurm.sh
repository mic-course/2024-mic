#!/bin/bash
#SBATCH --job-name=mic-vcf2maf-paired
#SBATCH --account=mic-2024
#SBATCH --mail-user=lwr11@duke.edu
#SBATCH --mail-type=END,FAIL
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10
#SBATCH --mem-per-cpu=18G
#SBATCH --output=./logs/mic-vcf2maf-paired-log-%j

wd=/work/mic-2024-gatk
anno=/hpc/group/mic-2024/references/GATK/hg38/v0
sif=/opt/apps/containers/community/mic-2024/dcibioinformaticsVEP-v2.0.sif

singularity exec \
  --containall \
  --no-home \
  --app samtools \
  --bind ${wd}:${wd}:rw \
  --bind ${anno}:${anno}:ro \
  ${sif} \
  /work/mic-2024-gatk/2024-mic/content/bioinformatics/vcf2maf/02_vcf2maf-paired.sh
