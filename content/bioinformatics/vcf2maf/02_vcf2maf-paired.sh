#!/bin/bash

# --- convert paired vcfs to MAF format
wd=/work/mic-2024-gatk
inputdir=${wd}/proc/somatic/stage5/paired
bcftools=/scif/apps/bcftools/bcftools-1.19/bcftools
vcf2maf=${wd}/2024-mic/content/bioinformatics/vcf2maf/vcf2maf_lwr.pl # path outside of container
veppath=/opt/vep/src/ensembl-vep
vepdata=${wd}/annotation/vepcache/ # release 110
fasta=/hpc/group/mic-2024/references/GATK/hg38/v0/Homo_sapiens_assembly38.fasta
outputdir=${wd}/proc/vcf2maf/paired
vcfs=${inputdir}/mutect2_filter/*/somatic_filtered.vcf.gz
legacy=false # indicate whether legacy or NF pipeline was used to generate somatic callsets;
# output file structure is slightly different for the two different workflow managers

N=8 # run in loops

# filtered non-PASS variants
for v in ${vcfs[@]};
do

    ((i=i%N)); ((i++==0)) && wait

    s="$(dirname -- $v)"

    if [ "$legacy" = true ]; then
      path=${s%/*}
    else
      path=${s}
    fi

    id=${path##*/}
    echo "ID: $id"
    echo "Now processing $id"

    mkdir -p ${outputdir}/pass_vcf/${id}
    mkdir -p ${outputdir}/maf/${id}
    mkdir -p ${outputdir}/logs/${id}

    logfile=${outputdir}/logs/${id}/vcf2maf.log
    tumor_library="$($bcftools view --header-only ${v} | grep '##tumor_sample' | awk '{gsub("^.*=", "")}1')" 
    normal_library="$($bcftools view --header-only ${v} | grep '##normal_sample' | awk '{gsub("^.*=", "")}1')" 

    {
    $bcftools view \
      --apply-filters 'PASS' \
      --output ${outputdir}/pass_vcf/${id}/${id}_somatic_filtered_PASS.vcf \
      ${v}

    perl ${vcf2maf} \
      --input-vcf ${outputdir}/pass_vcf/${id}/${id}_somatic_filtered_PASS.vcf \
      --output-maf ${outputdir}/maf/${id}/${id}_somatic_vep.maf \
      --tumor-id ${tumor_library} \
      --normal-id ${normal_library} \
      --ref-fasta ${fasta} \
      --ncbi-build GRCh38 \
      --species homo_sapiens \
      --vep-overwrite \
      --vep-path ${veppath} \
      --vep-data ${vepdata} \
      --cache-version 110 
    } > ${logfile} 2>&1

done 
