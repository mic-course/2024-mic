#!/usr/bin/env bash

VEP_SIF="/opt/apps/containers/community/mic-2024/dcibioinformaticsVEP-v2.0.sif"
WORK_DIR="/work/${USER}"

apptainer shell --bind /hpc/group/mic-2024,${WORK_DIR} $VEP_SIF
