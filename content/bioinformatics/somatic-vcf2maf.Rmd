---
title: 'Tutorial: Variant annotation (VEP/maftools)'
output:
  html_document:
    toc: true
    toc_float: true
  md_document:
    variant: markdown_github
    toc: true
editor_options: 
  chunk_output_type: inline
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
stdt <- date()
set.seed(123456)
```

# Goal

Annotate somatic variants using the Ensembl VEP tool and convert them to [MAF format](https://docs.gdc.cancer.gov/Data/File_Formats/MAF_Format/) for downstream analysis with `maftools`.

# Set up DCC Desktop

We need to use a different singularity container than the one we have been using in class to run `vcf2maf`. To run this singularity container and execute the commands in a bash shell, we need to start a DCC-OnDemand Desktop session by following the steps below:

1. Follow the same instructions that you used during the morning workshop to [Start up DCC Desktop in OOD](https://gitlab.oit.duke.edu/mic-course/2024-mic/-/blob/main/content/computing/dcc_ood/dcc_ood_desktop.md)
2. To open a bash shell, click on the "Terminal Emulator" icon at the bottom of the virtual desktop screen.
3. To orient yourself, type a few simple bash commands and move to your working directory. 

NOTE: I am assuming that you already set up the working directory `/work/${USER}/mic2024_proc` from previous workshops. To do this again, see `00_qc_and_trim.Rmd` line 86.

```{bash, eval = F}
pwd
cd /work/${USER}/mic2024_proc
ls

```

# Run vcf2maf on DCC Desktop

## Start a shell session 

Copy the lines below and paste them into your terminal on the virtual desktop. This will start a shell session inside of the singularity container `2024-mic-vep-simage_latest.sif`.

```{bash, eval = F}
export wd=/work/${USER}/mic2024_proc
export groupdir=/hpc/group/mic-2024
sif=/opt/apps/containers/community/mic-2024/2024-mic-vep-simage_latest.sif

singularity shell \
  --no-home \
  --app samtools \
  --bind ${wd}:${wd}:rw \
  --bind ${groupdir}:${groupdir}:ro \
  ${sif}
  
```

Your new shell inside the singularity container should have the prompt "Singularity>". If not, please ask for help.


## Define paths

Again, copy and paste the following into your terminal, do the same for the remaining bash chunks.

```{bash, eval = F}
# Path to the VEP software stored inside the container
veppath=/opt/vep/src/ensembl-vep

# Path to the VEP cache and reference genome fasta
vepcache=${groupdir}/references/vepcache
fasta=${groupdir}/references/GATK/hg38/v0/Homo_sapiens_assembly38.fasta

# Path to the input VCF
invcf=${groupdir}/downsampled_proc/somatic/somatic_filtered.vcf.gz

# Path to the output directory
outdir=${wd}/vcf2maf
mkdir -p ${outdir}

```

Check your output directory
```{bash, eval = F}
echo $outdir

```

## Select PASS variants from the VCF

```{bash, eval = F}
bcftools view \
  --apply-filters 'PASS' \
  --output ${outdir}/somatic_filtered_PASS.vcf \
  ${invcf}
  
```

## Get tumor and normal IDs from the VCF

```{bash, eval = F}
tumor_library="$(bcftools view --header-only ${invcf} | grep '##tumor_sample' | awk '{gsub("^.*=", "")}1')" 
normal_library="$(bcftools view --header-only ${invcf} | grep '##normal_sample' | awk '{gsub("^.*=", "")}1')" 

echo $tumor_library # SRR12415554
echo $normal_library # SRR12415556

```

## Run vcf2maf

NOTE! It's OK!: you will see the following warning message:
"WARNING: Unrecognized biotype "<biotype>". Assigning lowest priority!" This just indicates that newer versions of Ensembl VEP (which is called internally by vcf2maf) contains new biotypes/effects that have not yet been included in vcf2maf. For more info, see https://github.com/mskcc/vcf2maf/issues/349.

```{bash, eval = F}
perl /scripts/vcf2maf_mod.pl \
  --input-vcf ${outdir}/somatic_filtered_PASS.vcf \
  --output-maf ${outdir}/somatic_vep.maf \
  --tumor-id ${tumor_library} \
  --normal-id ${normal_library} \
  --ref-fasta ${fasta} \
  --ncbi-build GRCh38 \
  --species homo_sapiens \
  --vep-overwrite \
  --vep-path ${veppath} \
  --vep-data ${vepcache} \
  --cache-version 110

```




# Run vcf2maf with .sh script

```{bash}
cd 2024-mic/content/bioinformatics

chmod +x somatic-vcf2maf.sh

./somatic-vcf2maf.sh

```

# Transition to RStudio

Now that we have finished our task in DCC Desktop, you can close it by following the ["Shutting Down" directions](https://gitlab.oit.duke.edu/mic-course/2024-mic/-/blob/main/content/computing/dcc_ood/dcc_ood_desktop.md)

We will now need to start an RStudio session as you usually do for the course to complete this tutorial.

# Summarize and Visualize MAF in R

## Set up

### Libraries and paths
```{r, prep}
# libraries
library(tidyverse)
library(maftools)
library(RColorBrewer)
library(glue)
library(here)
library(tools)
library(fs)

# paths
"content/config.R" %>%
    here() %>%
    source()

# Path to the MAF you just created
downsamp_maf <- file.path(out_dir, "vcf2maf")

# Inputs:
# SRA sample metadata table
manifest.path <- file.path(data_dir, "sra_metadata.csv")
# Full set of *.maf files
full_maf <- file.path(group_dir, "proc/vcf2maf")



```

### Load the sample metadata table

```{r}
samples <- read.csv(manifest.path, header = TRUE) %>%
  separate(SampleName,
           into = c("Subject_ID", "Body_Site"),
           sep = "_",
           remove = FALSE) %>%
  mutate(Tumor = Body_Site != "GL") %>%
  select(sample = Run,
         Subject_ID,
         Body_Site,
         Tumor)

tumor.samples <- samples %>%
  filter(Tumor == TRUE)

nrow(tumor.samples) # 90

```

### Load MAF file generated in this tutorial

For the purpose of demonstration, we will load the MAF we just created into R but remember that this MAF was generated using downsampled data so it will not be very informative.

Identify the maf file
```{r}
downsamp_maf
mymaffile <- list.files(downsamp_maf,
                       pattern = "*.maf$",
                       full.names = TRUE,
                       recursive = TRUE)
mymaffile

```
Load the file into R
```{r}
mafobj.ds <- maftools::read.maf(maf = mymaffile)

mafobj.ds

```

Show the data slot. For MAF column definitions, see [MAF Format](https://docs.gdc.cancer.gov/Data/File_Formats/MAF_Format/)
```{r}
mafobj.ds@data

```
```{r}
mafobj.ds@data %>%
  unlist() %>%
  .[!is.na(.)]

```

### Load full MAF files

Also load the full MAF files that we explored during week 1's maftools workshop

```{r}
maffiles <- list.files(full_maf,
                       pattern = "*.maf$",
                       full.names = TRUE,
                       recursive = TRUE)

mafobj.list <- list()

for (i in 1:length(maffiles)) {
  
  myfile <- maffiles[i]
  mafobj <- maftools::read.maf(maf = myfile, verbose = F)
  mafobj.list[[i]] <- mafobj
  
}

mic.maf <- maftools::merge_mafs(mafobj.list)
mic.maf

```


## Visualization

### Overlay oncogenic signalling pathways

The `pathways` argument for `maftools::oncoplot` can pull pathways from two different databases:

1. Oncogenic signalling pathways (`sigpw`) [Sanchez-Vega 2018 Cell](https://pubmed.ncbi.nlm.nih.gov/29625050/): a curated list of 10 signalling pathways

2. Pan-cancer significantly mutated genes classified by their biological processes (`smgbp`) [Bailey 2018 Cell ](https://pubmed.ncbi.nlm.nih.gov/29625053/)

In the plot below, we selected the oncogenic signaling pathways.

```{r, fig.height = 10, fig.width = 7}
oncoplot(maf = mic.maf,
         pathways = 'sigpw', # plot oncogenic signalling pathways
         gene_mar = 12, # adjust margin width for gene names
         fontSize = 0.6) # adjust font size for gene names

```

### Transitions and transversions (TiTv)

`maftools::titv` classifies SNPs into [transitions and transversions](https://www.mun.ca/biology/scarr/Transitions_vs_Transversions.html) and returns a list of summarized tables which can be visualized using `maftools::plotTiTv`: 

```{r}

mic.titv <- titv(maf = mic.maf,
                 plot = FALSE, # don't automatically generate a plot
                 useSyn = TRUE) # include synonymous variants

plotTiTv(res = mic.titv)

```

### Compare mutation load against TCGA cohorts

`maftools::tcgaCompare` uses mutation load from TCGA [MC3](https://gdc.cancer.gov/about-data/publications/mc3-2017) for comparing mutation burden against 33 TCGA cohorts that represent 33 different cancer types.

Each cohort is plotted along the x-axis and sample size is plotted along the top of the plot. TMB is plotted on the y-axis. The example dataset we are using for the course is a mesothelioma dataset, so we will label our data as "MIC-MESO" in the plot.

```{r}
mic.mutload <- tcgaCompare(maf = mic.maf,
                           cohortName = "MIC-MESO",
                           logscale = TRUE,
                           capture_size = 60) # the target size (Mb) captured by the library prep kit used to generate these data

```



# Try it!

1. Generate an oncoplot that summarizes mutations based on pan-cancer driver genes grouped by biological processes. Show the top 5 gene sets -- hint: adjust the `pathways` and `topPathways` arguments. 


2. Calculate the median transition/transversion ratio by body site. Report how the ratio changes if you exclude synonymous variants. Why is this change expected?


3. Choose a new plot to try from the [maftools tutorial] (https://www.bioconductor.org/packages/release/bioc/vignettes/maftools/inst/doc/maftools.html)



# Session information

```{r, results='hold'}
sessionInfo()

print(glue("Start Time: ",stdt))
print(glue("End Time: ",date()))

```
