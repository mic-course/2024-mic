[HaplotypeCaller](https://gatk.broadinstitute.org/hc/en-us/articles/21905025322523-HaplotypeCaller)

[GenomicsDBImport](https://gatk.broadinstitute.org/hc/en-us/articles/21905031525019-GenomicsDBImport)

[GenotypeGVCFs](https://gatk.broadinstitute.org/hc/en-us/articles/21905118377755-GenotypeGVCFs)

[VariantFiltration](https://gatk.broadinstitute.org/hc/en-us/articles/21905081124891-VariantFiltration)

[VariantRecalibrator](https://gatk.broadinstitute.org/hc/en-us/articles/21905116362779-VariantRecalibrator)

[ApplyVQSR](https://gatk.broadinstitute.org/hc/en-us/articles/21905052461211-ApplyVQSR)

[CollectVariantCallingMetrics](https://gatk.broadinstitute.org/hc/en-us/articles/21905092095771-CollectVariantCallingMetrics-Picard)

[VCF Format](https://samtools.github.io/hts-specs/VCFv4.2.pdf)
