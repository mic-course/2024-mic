#!/usr/bin/env python3


# In FORMAT column (;-sep), get CQS=XXXX,YYYY,...
# In header, get ##INFO=<ID=CSQ,Number=.,Type=String,Description="Consequence annotations from Ensembl VEP. Format: A|B|C|...">
# Pull Format from this entry, make hash for each CQS entry

from enum import IntEnum
import argparse as ap
import gzip as gz
import re
import sys

class VCF(IntEnum):
  """
  Standard VCF Fields before Sample entries
  """
  CHR = 0
  POS = 1
  ID = 2
  REF = 3
  ALT = 4
  QUAL = 5
  FILTER = 6
  INFO = 7
  FORMAT = 8
  

class Refalt():
  """
  Convenience class for shifting ref/alt pairs by coordinate difference
  Keeps Alternate alleles in an enumeration so we don't lose the Allele number
  when removing '*' entries
  """
  def __init__(self, ref, alts, i=0):
    self.ref = ref[i:] if i < len(ref) else '-'
    self.alts = [(it[0], it[1][i:]) if i < len(it[1]) else (it[0], '-') for it in alts if i <= len(it[1]) and it[1] != '*']

  def shift(self, i):
    return Refalt(self.ref, self.alts, i)
    
    
"""
REGEX for pulling CSQ format
This will support ANY chosen CSQ output for VEP
"""
format_re=re.compile(r'##INFO=<ID=CSQ,Number=.,Type=String,Description="Consequence annotations from Ensembl VEP. Format: (.*)">')


def get_format(fh):
  """
  Extract format information from header INFO line
  """  
  line = next(fh)
  m = re.match(format_re, line)
  while not m:
    line = next(fh)
    m = re.match(format_re, line)
    if line[0] != "#":
       print("No CSQ field defined in VCF header. Exiting...")
       exit(1)

  return m[1].split("|")     


def get_so_terms(fh):
  """
  Reads in SO_term table, return consequence ranking
  """
  header = [it.strip('"') for it in next(fh).strip().split(",")]
  dat = (line.strip().split(",") for line in fh)
  so_terms = []
  for x in dat:
    so_terms.append({item: x[idx] for idx, item in enumerate(header)})
  return [x["cons"].strip('"') for x in sorted(so_terms, key=lambda x: int(x["rank"]))]


def get_csq(so_terms, fmt, item):
  """
  Reads CSQs from record
  creates new columns (INFO w/o CSQs, CSQs)
  """
  csq = []
  col = item[7].split(";")
  rmidx = None
  for idx, x in enumerate(col):
    if x[0:4] == "CSQ=":
      rmidx = idx
      for cq in x[4:].split(","):
        item = {a:cq.split("|")[idx] for idx, a in enumerate(fmt)}
        item['cons'], item['rank'] =  sorted([(y, so_terms.index(y)) for y in item["Consequence"].split("&") if y], key=lambda x: x[1])[0]
        csq.append(item)
      break
  return ";".join(col[:rmidx] + col[rmidx+1:]), csq


def extract_gts(gtitems, remidx):
  """
  Count all present genotypes
  """
  gtvals = [it.split(":")[0] for idx, it in enumerate(gtitems) if idx not in remidx]
  gtnames = set(gtvals)
  return {gt:gtvals.count(gt) for gt in gtnames}


def gt_groups(gts):
  """  
  categorize and count genotypes (homRef, het, AltAlt, miss)
  """
  gtgroups = {'homRef': 0, 'het':0, 'altAlt': 0, 'miss': 0}
  for key, val in gts.items():
    try:
      a, b = key.split('/') if '/' in key else key.split('|')
    except ValueError: # key == '.' sometimes
      gtgroups['miss'] += val
      continue
    
    try:
      a = int(a)
      b = int(b)
      if a == 0 or b == 0:
        if a == 0 and b == 0:
          gtgroups['homRef'] += val
        else:
          gtgroups['het'] += val
      else:
        gtgroups['altAlt'] += val
    except:
      gtgroups['miss'] += val
  return (gtgroups["homRef"], gtgroups["het"], gtgroups["altAlt"], gtgroups["miss"])


def qprint(*args, **kwargs):
  """
  wrapped print function that converts everything to quoted string for slightly safer CSV output
  could replace with actual python csv module
  """
  print(*(f'"{it}"' for it in args), **kwargs)

  
def extract_csq(so_terms, fmt, header, data, of, remidx):
  """
  Reads in each record from VCF
  prints modified entry with a single consequence for
  each CSQ entry/transcript
  """
  qprint(*header[:max(VCF)], 'VCF_POS', *fmt, 'rank', 'allele_number', 'GT_counts', 'homRef', 'het',
         'altAlt', 'miss', sep=',', file=of)
  tot = 0
  for item in data:
    new_INFO, csq = get_csq(so_terms, fmt, item)
    tot += len(csq)
    refalt = Refalt(item[VCF.REF], enumerate(item[VCF.ALT].split(',')))
    for curr in csq:
        gts = extract_gts(item[max(VCF).value+1:], remidx)
        gtgroups = gt_groups(gts)
        if gtgroups[1] + gtgroups[2] > 0:          
          locus = curr['Location'].split(':')[1].split('-')
          locus = locus[0] if curr['REF_ALLELE'] else locus[1]
          ra = refalt.shift(int(locus)-int(item[VCF.POS]))
          dup = False
          for allele_index, alt in ra.alts:
            if alt == curr['Allele']:
              if dup:
                print("DUP:", item[VCF.CHR], item[VCF.POS], file=sys.stderr)
              dup = True
              qprint(item[VCF.CHR], locus, item[VCF.ID], ra.ref if ra.ref else '-',                   
                     alt, *item[VCF.QUAL:VCF.INFO], new_INFO, item[VCF.POS],
                     *[curr[item] for item in fmt], curr['rank'], allele_index+1,                     
                      ";".join([f"{key}:{val}" for key, val in gts.items()]),                        
                     *gtgroups, sep=",", file=of)
  print(f'# transcripts processed: {tot}', file=sys.stderr)

  
def extract(fh, so_terms, of, rem):
  """
  Reads header, extracts data from VCF
  """
  fmt = get_format(fh)  
  header = next(fh)
  while header[0:2] == "##":
    header = next(fh)
  header = header.strip().strip("#").split()
  remidx = [idx-max(VCF).value-1 for idx, it in enumerate(header) if it in rem]
  extract_csq(so_terms, fmt, header, (line.strip().split() for line in fh), of, remidx)
  
      
def main(args, of):
  with open(args["table"], 'r') as fh:
    so_terms = get_so_terms(fh)
  try:
    with gz.open(args["file"], 'rt') as fh:
      extract(fh, so_terms, of, args['rem'])
  except OSError:
    with open(args["file"], 'r') as fh:
      extract(fh, so_terms, of, args['rem'])

      
parser = ap.ArgumentParser()
parser.add_argument("file", type=str, help="input vcf/vcf.gz file")
parser.add_argument("table", type=str, help="table of ranked SO_terms")
parser.add_argument('-o,--outfile', dest='o', type=str, help="output filename")
parser.add_argument('-s,--samples-removed', dest='rem', type=str, nargs='*', help='list of samples to exclude')
if __name__ == '__main__':
  args = vars(parser.parse_args())
  if args['o']:
    with open(args['o'], 'w') as of:
      main(args, of)
  else:
    main(args, sys.stdout)
