#!/bin/bash
# inputs: genomes wdir input_vcf 
veppath=/opt/vep/src/ensembl-vep
sif=/work/yd168/nci-r25-full-runs/containers/dcibioinformaticsVEP-v2.0.sif
# md5sum for the gnomAD3.1.vcf.gz
# 324e2e08840293d0d0e0a5e3a9c70ae4 gnomAD3.1.vcf.gz
# 863db39d517c9bf9067dd90e67e8d8bc  gnomAD3.1.vcf.gz.tbi
# --custom file=/genomes/gnomAD3.1.vcf.gz,short_name=gnomADg,format=vcf,type=exact,coords=0,fields=AF_AFR%AF_AMR%AF_ASJ%AF_EAS%AF_FIN%AF_NFE%AF_OTH
# --af_gnomad = --af_gnomade 
# So the genome is called from the full reference and the exome is called from te cache. 

singularity exec \
  --containall \
  --no-home \
  --bind ${wdir}:/workdir/:rw \
  --bind ${genomes}:/genomes/:ro \
  --bind ${input_vcf}:/input/ \
  ${sif} \
  ${veppath}/vep \
  --fork 4 \
  --vcf \
  --custom /genomes/gnomAD3.1.vcf.gz,gnomADg,vcf,exact,0,AF,AF-afr,AF-amr,AF-asj,AF-eas,AF-fin,AF-nfe,AF-oth,AF-sas \
  --input_file /input/snp_recal.vcf.gz  \
  --output_file /workdir/VEP/germline-vep.vcf  \
  --dir_cache /workdir/VEP/vepcache/ \
  --symbol \
  --af_1kg \
  --af_gnomad \
  --polyphen s \
  --sift s \
  --hgvs \
  --show_ref_allele \
  --assembly GRCh38 \
  --species homo_sapiens \
  --cache \
  --cache_version 110 \
  --fields Uploaded_variation,Allele,Location,Consequence,IMPACT,SYMBOL,Gene,Feature_type,Feature,BIOTYPE,EXON,INTRON,HGVSc,HGVSp,cDNA_position,CDS_position,Protein_position,Amino_acids,Codons,Existing_variation,REF_ALLELE,DISTANCE,STRAND,FLAGS,SYMBOL_SOURCE,HGNC_ID,SOURCE,SIFT,PolyPhen,HGVS_OFFSET,AFR_AF,AMR_AF,EAS_AF,EUR_AF,SAS_AF,gnomAD_AF,gnomAD_AFR_AF,gnomAD_AMR_AF,gnomAD_ASJ_AF,gnomAD_EAS_AF,gnomAD_FIN_AF,gnomAD_NFE_AF,gnomAD_OTH_AF,gnomAD_SAS_AF,CLIN_SIG,SOMATIC,PHENO,gnomADg,gnomADg_AF,gnomADg_AF-afr,gnomADg_AF-amr,gnomADg_AF-asj,gnomADg_AF-eas,gnomADg_AF-fin,gnomADg_AF-nfe,gnomADg_AF-oth,gnomADg_AF-sas
