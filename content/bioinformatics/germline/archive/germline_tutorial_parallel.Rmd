---
title: 'Germline tutorial'
output:
  html_document:
    toc: true
    toc_float: true
  md_document:
    variant: markdown_github
    toc: true
editor_options: 
  chunk_output_type: inline
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Goals

- Call germline SNPs and indels via local re-assembly of haplotypes for each sample using `HaplotypeCaller` (germline stage4)
- Import single-sample GVCFs into GenomicsDB before joint genotyping using `GenomicsDBImport` (germline stage5)
- Perform joint genotyping on one or more samples pre-called with HaplotypeCaller using `GenotypeGVCFs` (germline stage5)
- Gathers multiple VCF files from a scatter operation into a single VCF file using `GatherVcfsCloud` (germline stage6)
- Sort and index the VCF file using bcftools `sort` and `index` (germline stage6)
- Filter out variants ExcessHet > 54.69 using `VariantFiltration` (germline stage7)
    + ExcessHet: Phred-scaled p-value for exact test of excess heterozygosity
    + The higher the score, the higher the chance that the variant is a technical artifact or that there is consanguinuity among the samples.
- Remove all genotype information from it while retain all site level information, including annotations based on genotypes, for the VCF file using `MakeSitesOnlyVcf` (germline stage8)
- Calculate VQSLOD tranches for indels and snps using `VariantRecalibrator` (germline stage8)
- Filter indels on VQSLOD for indels and snps using `ApplyVQSR` (germline stage8)
    + VQSLOD: the score that measures the probability that each known and novel variation call is real. 
    + Variant Quality Score Recalibration (VQSR) 
- Collect variant calling metrics using `CollectVariantCallingMetrics` (germline stage9)

# Setup

First we load libraries.

```{r, libraries}

library(tidyverse)
library(glue)
library(readr)
library(fs)
library(dplyr)
library(tibble)
#library(Biostrings) # need to add to sif
library(tools)
library(stringr)
library(here)

```

## Paths, Directories, and Shell Variables
To keep the code readable and portable, it is nice to assign paths to variables.  We also need to use the R `Sys.setenv` command to make shell variables that can be used in the bash chunks below.


```{r, files_and_directories}
"content/config.R" %>%
    here() %>%
    source()

# make directory for output
if (!dir_exists(germline_proc_dir)) {dir_create(germline_proc_dir)}

# make directory for temporary files
if (!dir_exists(tmp_dir)) {dir_create(tmp_dir)}

germline_proc_dir
```

# HaplotypeCaller 

HaplotypeCaller on the preprocessed downsampled normal sample. (Pending the normal sample)

```{bash}
echo $PREPROC_DIR
echo $GERMLINEPROC_DIR
```

**This takes ~2-3 hrs with 10 cores and ~4GB**

```{bash}
recalbam=${PREPROC_DIR}/recal.bam

gatk --java-options -Xmx24G HaplotypeCaller \
  --input ${recalbam} \
  --output ${GERMLINEPROC_DIR}/output.g.vcf.gz \
  -ERC GVCF \ 
  --reference ${GENOMEFA} \
  --dbsnp ${DBSNP} \
  --intervals ${TARGETS} \
  --tmp-dir ${TMP_DIR}

# -ERC, --emit-ref-confidence: Mode for emitting reference confidence scores
# GVCF is sometimes used simply to describe VCFs that contain a record for every position in the genome (or interval of interest) regardless of whether a variant was detected at that site or not.

```

# GenomicsDBImport

## Split the interval file in sub-intervals for parallel runs 

```{bash}

mkdir ${GERMLINEPROC_DIR}/subintervals

gatk --java-options "-Xmx16G -Xms8G" SplitIntervals \
  --reference ${GENOMEFA} \
  --intervals ${TARGETS} \
  --scatter-count 10 \
  --subdivision-mode BALANCING_WITHOUT_INTERVAL_SUBDIVISION_WITH_OVERFLOW \
  --output ${GERMLINEPROC_DIR}/subintervals \
  --tmp-dir ${TMP_DIR}

```

## Generated the map and sub-interval file 

```{r}

# Write the sample map file to a text file 
outmap <- file.path(germline_proc_dir, "sample.map") # output path for sample map file
samples <- list.files(germline_proc_dir,
                      pattern = "output.g.vcf.gz$",
                      full.names = TRUE,
                      recursive = TRUE)
sm <- 
  data.frame(samples) %>%
  separate(samples,
           into = c(NA, NA, NA, NA, NA, NA,
                    NA, NA, "library", NA),
           sep = "/",
           remove = FALSE) %>%
  relocate(library) 
write.table(sm, 
            outmap,
            sep = "\t",
            quote = FALSE,
            row.names = FALSE,
            col.names = FALSE)

# Write the sub-interval files 
subints <- file.path(germline_proc_dir,"subintervals") 
outintervals <- file.path(germline_proc_dir, "intervals.csv") # output path for stage5.sub_intervals

subs <- list.files(subints,
                   pattern = "*.interval_list",
                   full.names = TRUE)

subIDs <- paste("subint", c(0:9), sep = "") # assign sub-interval IDs so they can be used as the "uid"

subs.df <- data.frame(subIDs, subs)

write.csv(subs.df, 
          row.names = FALSE, 
          quote = FALSE,
          outintervals)


```

## GenomicsDBImport for each sub-interval

Joint import for all samples. 

```{bash}
echo $GERMLINEPROC_DIR
echo $GENOMEFA
```

**This takes ~100 mins with 10 cores and ~1GB**

```{bash}

for intidx in 0 1 2 3 4 5 6 7 8 9
do 
  gatk --java-options -Xmx16G -Xms8G GenomicsDBImport \
    --reference ${GENOMEFA} \
    --intervals "${GERMLINEPROC_DIR}/subintervals/000${intidx}-scattered.interval_list" \
    --genomicsdb-workspace-path ${GERMLINEPROC_DIR}/subint${intidx}/ \
    --overwrite-existing-genomicsdb-workspace true \
    --genomicsdb-shared-posixfs-optimizations true \
    --merge-input-intervals true \
    --bypass-feature-reader true \
    --sample-name-map ${GERMLINEPROC_DIR}/sample.map \
    --tmp-dir ${TMP_DIR}
done

```

## GenotypeGVCFs for each sub-interval

Joint genotyping for all samples. 

```{bash}
echo ${GERMLINEPROC_DIR}/subintervals/
echo ${GENOMEFA}
```

**This takes ~1.5 hrs with 10 cores and ~2GB**

```{bash}

for intidx in 0 1 2 3 4 5 6 7 8 9
do
  gatk --java-options -Xmx24G GenotypeGVCFs \
    --reference ${GENOMEFA} \
    --variant ${GERMLINEPROC_DIR}/subint${intidx} \
    --genomicsdb-shared-posixfs-optimizations true \
    --output ${GERMLINEPROC_DIR}/subint${intidx}/output.vcf.gz \
    --tmp-dir ${TMP_DIR}
done 

```

# Merge and sort the joint vcf file 

**This takes within 5 mins with 5 cores and ~200MB-300MB**

```{bash}

gatk --java-options -Xmx24G GatherVcfsCloud \
  --input ${GERMLINEPROC_DIR}/subint0/output.vcf.gz \
  --input ${GERMLINEPROC_DIR}/subint1/output.vcf.gz \
  --input ${GERMLINEPROC_DIR}/subint2/output.vcf.gz \
  --input ${GERMLINEPROC_DIR}/subint3/output.vcf.gz \
  --input ${GERMLINEPROC_DIR}/subint4/output.vcf.gz \
  --input ${GERMLINEPROC_DIR}/subint5/output.vcf.gz \
  --input ${GERMLINEPROC_DIR}/subint6/output.vcf.gz \
  --input ${GERMLINEPROC_DIR}/subint7/output.vcf.gz \
  --input ${GERMLINEPROC_DIR}/subint8/output.vcf.gz \
  --input ${GERMLINEPROC_DIR}/subint9/output.vcf.gz \
  --output ${GERMLINEPROC_DIR}/merged_variants.vcf.gz \
  --create-output-variant-index true \
  --tmp-dir ${TMP_DIR}

bcftools sort \
  --output ${GERMLINEPROC_DIR}/merged_sort_variants.vcf.gz \
  --output-type z \
  --temp-dir ${TMP_DIR} \
  ${GERMLINEPROC_DIR}/merged_variants.vcf.gz

bcftools index \
  --tbi \
  --output ${GERMLINEPROC_DIR}/merged_sort_variants.vcf.gz.tbi \
  ${GERMLINEPROC_DIR}/merged_sort_variants.vcf.gz

```

# Hard filter

**This takes within 1 min with 5 cores and ~1GB**

This step can be ignored if you have a small sample size. Added for practice.

```{bash}

gatk --java-options -Xmx24G VariantFiltration \
  --variant ${GERMLINEPROC_DIR}/merged_sort_variants.vcf.gz \
  --output ${GERMLINEPROC_DIR}/filt.vcf.gz \
  --filter-expression "ExcessHet > 54.69" \
  --filter-name "ExcessHet" \
  -OVI  \
  --tmp-dir ${TMP_DIR}

```

# Filter a cohort callset 

## Create site-only VCF to speed up the analysis 

**This takes within 1 min with 10 cores and ~1.5GB**

```{bash}

java -Xmx24G -jar /scif/apps/picardtools/picard.jar MakeSitesOnlyVcf \
  --INPUT ${GERMLINEPROC_DIR}/filt.vcf.gz \
  --OUTPUT ${GERMLINEPROC_DIR}/sites.vcf.gz \
  --TMP_DIR ${TMP_DIR}

```

## VariantRecalibrator

* For Indels and SNPs separately. 
* For Indels: **This takes ~5 mins with 10 cores and ~1.5GB**
* For SNPs: **This takes within 1 min with 10 cores and ~1.5GB**

```{bash}

# Indel =====
gatk --java-options -Xmx24G VariantRecalibrator \
  --variant ${GERMLINEPROC_DIR}/sites.vcf.gz \
  --trust-all-polymorphic \
  --truth-sensitivity-tranche 100.0 \
  --truth-sensitivity-tranche 99.95 \
  --truth-sensitivity-tranche 99.9 \
  --truth-sensitivity-tranche 99.5 \
  --truth-sensitivity-tranche 99.0 \
  --truth-sensitivity-tranche 97.0 \
  --truth-sensitivity-tranche 96.0 \
  --truth-sensitivity-tranche 95.0 \
  --truth-sensitivity-tranche 94.0 \
  --truth-sensitivity-tranche 93.5 \
  --truth-sensitivity-tranche 93.0 \
  --truth-sensitivity-tranche 92.0 \
  --truth-sensitivity-tranche 91.0 \
  --truth-sensitivity-tranche 90.0 \
  --use-annotation FS \
  --use-annotation ReadPosRankSum \
  --use-annotation MQRankSum \
  --use-annotation QD \
  --use-annotation SOR \
  --mode INDEL \
  --max-gaussians 4 \
  --resource:mills,known=false,training=true,truth=true,prior=12.0 ${INDEL2} \
  --resource:dbsnp,known=true,training=false,truth=false,prior=2.0 ${DBSNP} \
  --output ${GERMLINEPROC_DIR}/indels.recal.vcf \
  --tranches-file ${GERMLINEPROC_DIR}/indels.tranches \
  --tmp-dir ${TMP_DIR}
    
# SNP ====
gatk --java-options -Xmx24G VariantRecalibrator \
  --variant ${GERMLINEPROC_DIR}/sites.vcf.gz \
  --trust-all-polymorphic \
  --truth-sensitivity-tranche 100.0 \
  --truth-sensitivity-tranche 99.95 \
  --truth-sensitivity-tranche 99.9 \
  --truth-sensitivity-tranche 99.5 \
  --truth-sensitivity-tranche 99.0 \
  --truth-sensitivity-tranche 97.0 \
  --truth-sensitivity-tranche 96.0 \
  --truth-sensitivity-tranche 95.0 \
  --truth-sensitivity-tranche 94.0 \
  --truth-sensitivity-tranche 93.5 \
  --truth-sensitivity-tranche 93.0 \
  --truth-sensitivity-tranche 92.0 \
  --truth-sensitivity-tranche 91.0 \
  --truth-sensitivity-tranche 90.0 \
  --use-annotation QD \
  --use-annotation MQ \
  --use-annotation MQRankSum \
  --use-annotation ReadPosRankSum \
  --use-annotation FS \
  --use-annotation SOR  \
  --mode SNP \
  --max-gaussians 6 \
  --resource:hapmap,known=false,training=true,truth=true,prior=15.0 ${HAMPMAP} \
  --resource:omni,known=false,training=true,truth=true,prior=12.0 ${OMNI} \
  --resource:1000G,known=false,training=true,truth=false,prior=10.0 ${PHASE} \
  --resource:dbsnp,known=true,training=false,truth=false,prior=2.0 ${DBSNP} \
  --output ${GERMLINEPROC_DIR}/snps.recal.vcf \
  --tranches-file ${GERMLINEPROC_DIR}/snps.tranches \
  --tmp-dir ${TMP_DIR}

```

## Apply VQSR

* For Indels and SNPs separately. 
* For Indels: **This takes ~8 mins with 10 cores and ~1.5GB**
* For SNPs: **This takes within 1 min with 10 cores and ~1.5GB**

```{bash}
# Indel ====
gatk --java-options -Xmx24G ApplyVQSR \
    --variant ${GERMLINEPROC_DIR}/filt.vcf.gz \
    --recal-file ${GERMLINEPROC_DIR}/indels.recal.vcf \
    --tranches-file ${GERMLINEPROC_DIR}/indels.tranches \
    --truth-sensitivity-filter-level 99.7 \
    --mode INDEL \
    -OVI \
    --output ${GERMLINEPROC_DIR}/indel_recal.vcf.gz \
    --tmp-dir ${TMP_DIR}

# SNP
gatk --java-options -Xmx24G ApplyVQSR \
    --variant ${GERMLINEPROC_DIR}/indel_recal.vcf.gz \
    --recal-file ${GERMLINEPROC_DIR}/snps.recal.vcf \
    --tranches-file ${GERMLINEPROC_DIR}/snps.tranches \
    --truth-sensitivity-filter-level 99.7 \
    --mode SNP \
    -OVI \
    --output ${GERMLINEPROC_DIR}/snp_recal.vcf.gz \
    --tmp-dir ${TMP_DIR}

```

# Collect variant calling metrics

* For the raw VCF file
* For the hard filtered VCF file
* For the hard+VQSR filtered VCF file

The output will be:

* raw.variant_calling_summary_metrics
* raw.variant_calling_detail_metrics
* filt.variant_calling_summary_metrics
* filt.variant_calling_detail_metrics
* final.variant_calling_summary_metrics
* final.variant_calling_detail_metrics

```{bash}

# Raw
java -Xmx24G -jar /scif/apps/picardtools/picard.jar CollectVariantCallingMetrics \
  --INPUT ${GERMLINEPROC_DIR}/merged_sort_variants.vcf.gz \
  --DBSNP ${DBSNP} \
  --OUTPUT ${GERMLINEPROC_DIR}/raw \
  --THREAD_COUNT 10 \
  --TARGET_INTERVALS ${TARGETS} \
  --TMP_DIR ${TMP_DIR}

# Hard filter
java -Xmx24G -jar /scif/apps/picardtools/picard.jar CollectVariantCallingMetrics \
  --INPUT ${GERMLINEPROC_DIR}/filt.vcf.gz \
  --DBSNP ${DBSNP} \
  --OUTPUT ${GERMLINEPROC_DIR}/ \
  --THREAD_COUNT 10 \
  --TARGET_INTERVALS ${TARGETS} \
  --TMP_DIR ${TMP_DIR}

# Hard filter and VQSR filtered
java -Xmx24G -jar /scif/apps/picardtools/picard.jar CollectVariantCallingMetrics \
  --INPUT ${GERMLINEPROC_DIR}/snp_recal.vcf.gz \
  --DBSNP ${DBSNP} \
  --OUTPUT ${GERMLINEPROC_DIR}/final \
  --THREAD_COUNT 10 \
  --TARGET_INTERVALS ${TARGETS} \
  --TMP_DIR ${TMP_DIR}
```

