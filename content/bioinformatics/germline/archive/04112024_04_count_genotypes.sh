#!/usr/bin/env python3

import argparse as ap
from enum import IntEnum
import re


class VCF(IntEnum):
  """
  Standard VCF Fields before Sample entries
  """
  CHR = 0
  POS = 1
  ID = 2
  REF = 3
  ALT = 4
  QUAL = 5
  FILTER = 6
  INFO = 7
  FORMAT = 8


def extract_gts(gtitems):
  """
  Count all present genotypes
  """
  gtvals = [it.split(":")[0] for it in gtitems]
  gtnames = set(gtvals)
  return {gt:gtvals.count(gt) for gt in gtnames}


def gt_groups(gts):
  """  
  categorize and count genotypes (homRef, het, AltAlt, miss)
  """
  gtgroups = {'homRef': 0, 'het':0, 'altAlt': 0, 'miss': 0}
  for key, val in gts.items():
    try:
      a, b = key.split('/') if '/' in key else key.split('|')
    except ValueError: # key == '.' sometimes
      gtgroups['miss'] += val
      continue
    
    try:
      a = int(a)
      b = int(b)
      if a == 0 or b == 0:
        if a == 0 and b == 0:
          gtgroups['homRef'] += val
        else:
          gtgroups['het'] += val
      else:
        gtgroups['altAlt'] += val
    except:
      gtgroups['miss'] += val
  return (gtgroups["homRef"], gtgroups["het"], gtgroups["altAlt"], gtgroups["miss"])


mre = re.compile(r'^0/[1-9]+:|^[1-9]+/[1-9]+:|^[1-9]+/0:')

def main(args):
  with open(args['file'], 'r') as fh:
    header = next(fh)
    while header[0:2] == "##":
      header = next(fh)
    header = header.strip().strip("#").split()
    counts = {sample: 0 for sample in header[9:]}
    for line in fh:
      line = line.strip().split()
      gts = extract_gts(line)		
      grps = gt_groups(gts)
      if grps[1] + grps[2] == 1:
        for idx, item in enumerate(line[9:]):
          if re.match(mre, item):
            counts[header[9+idx]] += 1

  with open(args['outfile'], 'w') as of:
    for key, val in counts.items():
      print(key, val, file=of) 

  
parser = ap.ArgumentParser()
parser.add_argument('file')
parser.add_argument('outfile')
if __name__=='__main__':
   main(vars(parser.parse_args()))
