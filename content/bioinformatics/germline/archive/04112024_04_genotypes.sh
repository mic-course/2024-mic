#!/usr/bin/env python3

import argparse as ap
import gzip as gz


def extract(fh):
  header = fh.readline()
  while header[:2] == '##':
    header = fh.readline()
  header = header.strip().split()
  geno = {idx:[sample] for idx, sample in enumerate(header[9:])}
  locs = []
    
  for line in fh:
    line = line.strip().split()
    locs.append(f'{line[0]}:{line[1]}')
    for i in range(len(line[9:])):
       geno[i].append(line[9+i].replace(',', ';'))        #.split(":")[0])

  return geno, locs

  
def main(args):
  try:
    with gz.open(args['file'], 'rt') as fh:
      geno, locs = extract(fh)
  except:
    with open(args['file'], 'r') as fh:
      geno, locs = extract(fh)

  with open(args['outfile'], 'w') as of:
    print('sample', *locs, sep=',', file=of)
    for key, vals in geno.items():
      print(*vals, sep=',', file=of)

      
parser = ap.ArgumentParser()
parser.add_argument('file')
parser.add_argument('outfile')
if __name__ == '__main__':
  main(vars(parser.parse_args()))
