srun \
  --account=mic-2024 \
  --partition=chsi-high \
  --nodes=1 \
  --ntasks-per-node=1 \
  --cpus-per-task=5 \
  --pty bash -i

sif=/opt/apps/containers/community/mic-2024/dcibioinformaticsHTS-v0.9.sif
germline=/hpc/group/mic-2024/proc/germline/stage8/VQSR_SNP/merged
wd=/work/lwr11
out=${wd}/mic-2024-proc/germline/gviz

##INFO=<ID=AF,Number=A,Type=Float,Description="Allele Frequency, for each ALT allele, in the same order as listed">
singularity exec \
  --containall \
  --no-home \
  --app bcftools \
  --bind ${germline}:${germline}:ro \
  --bind ${wd}:${wd}:rw \
  ${sif} \
  bcftools query \
  --format '%CHROM\t%POS\t%REF\t%ALT\t[%SAMPLE=%AD:%DP;]\n' \
  ${germline}/snp_recal.vcf.gz \
  > ${out}/snp_recal_metadata.tsv