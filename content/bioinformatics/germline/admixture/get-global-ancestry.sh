srun \
  --account=mic-2024 \
  --partition=chsi-high \
  --nodes=1 \
  --ntasks-per-node=1 \
  --cpus-per-task=5 \
  --pty bash -i

# --- [1] generate plink file set from germline.vcf
# --- and apply some basic QC filters:
# --- remove variants with MAF < 0.05
# --- remove variants with missing call rates > 0.05
sif=/opt/apps/containers/community/mic-2024/dcibioinformaticsHTS-v0.9.sif
germline=/hpc/group/mic-2024/proc/germline/stage8/VQSR_SNP/merged
wd=/work/lwr11

singularity exec \
  --containall \
  --no-home \
  --app plink \
  --bind ${germline}:${germline}:ro \
  --bind ${wd}:${wd}:rw \
  ${sif} \
  plink \
  --vcf ${germline}/snp_recal.vcf.gz \
  --maf 0.05 \
  --geno 0.05 \
  --make-bed \
  --out ${wd}/mic-2024-proc/germline/plink/mic-2024-germline-snps

# --- [2] run admixture
# -1- install admixture (this is not installed in any of our containers!)
cd /work/lwr11/software
wget https://dalexander.github.io/admixture/binaries/admixture_linux-1.3.0.tar.gz
tar -xzf admixture_linux-1.3.0.tar.gz
mv dist/admixture_linux-1.3.0 .
rm admixture_linux-1.3.0.tar.gz
rm -rf dist/

# -2- run admixture!
# manual: https://dalexander.github.io/admixture/admixture-manual.pdf
admixture=/work/lwr11/software/admixture_linux-1.3.0/admixture
plinkfiles=/work/lwr11/mic-2024-proc/germline/plink
out=/work/lwr11/mic-2024-proc/germline/admixture

# -2.1- identify reasonable value for K (# of ancestral populations)
for K in 1 2 3 4 5; \
  do ${admixture} --cv ${plinkfiles}/mic-2024-germline-snps.bed $K \
  | tee ${out}/log${K}.out; done

grep -h CV ${out}/log*.out
# CV error (K=1): 0.64228 <- lowest
# CV error (K=2): 0.80803
# CV error (K=3): 1.07160
# CV error (K=4): 1.36968
# CV error (K=5): 1.86837

# -2.2- run admixture with K = 1
cd ${out}
${admixture} ${plinkfiles}/mic-2024-germline-snps.bed 1

# -2.3- run admixture with K = 3
cd ${out}
${admixture} ${plinkfiles}/mic-2024-germline-snps.bed 3