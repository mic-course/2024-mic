# Start IGV
1. [Start up DCC Desktop in OOD](../computing/dcc_ood/dcc_ood_desktop.md)
2. Open a terminal in DCC Desktop by clicking on the Terminal icon (black box surrounded by grey border in the dock at the bottom of the desktop)
3. Run IGV by typing `/hpc/group/mic-2024/igv_stuff/run_igv` then **Return** in the terminal.

> Note that the terminal is case-sensitive

# Load Files
Once IGV is running do the following within IGV:
1. **Genome Sequence** 
  1. From the menu select **Genomes**->**Load Genome From File**
  2. In the File Browser :
    - click **Other Locations**
    - click **Computer**
    - double click **hpc**
    - double click **group**
    - double click **mic-2024**
    - double click **igv_stuff**
    - double click **Homo_sapiens_assembly38.fasta**
2. **Annotation**
  1. From the menu select **File**->**Load From File**
  2. In the File Browser :
    - click **Other Locations**
    - click **Computer**
    - double click **hpc**
    - double click **group**
    - double click **mic-2024**
    - double click **igv_stuff**
    - double click **gencode.v45.basic.annotation.gtf.gz**
3. **BAMs**
  1. From the menu select **File**->**Load From File**
  2. The File Browser should already be in **igv_stuff** from loading the GTF
  3. Select all the BAM files by control-clicking on each one (hold down the **Control** button while you click with the mouse)
  4. Click on **Open** to load them.

  
# Things to demonstrate

1. Teleport to DNAH1
2. Right click menu
    - View as Pairs
    - Collapsed/Expanded/Squished
    - Color alignments by -> read strand
3. Click on read to view details
4. Coverage plot (show scaling)
5. Zoom in by dragging on navigator
5. Zoom in with zoom slider
6. Drag window to scroll
