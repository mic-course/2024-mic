-   [DCC OOD Container Specs](#dcc-ood-container-specs)
-   [Set up](#set-up)
    -   [Libraries](#libraries)
    -   [Data](#data)
-   [Gviz pileup](#gviz-pileup)
    -   [BAP1](#bap1)
-   [Zoom out from the BAP1 mutation](#zoom-out-from-the-bap1-mutation)
-   [Session information](#session-information)

# DCC OOD Container Specs

-   CPUs: 2
-   RAM: 20G

# Set up

## Libraries

``` r
startdt <- date() # start time
tblno <- 0 # table index
figno <- 0 # table index

# library(tidyverse)
library(Gviz)
library(data.table)
library(RColorBrewer)
library(rtracklayer)
library(GenomicAlignments)
library(BSgenome.Hsapiens.UCSC.hg38)
library(maftools)
library(glue)
library(dplyr)
library(here)
library(stringr)
library(tidyr)
```

Then we load the paths and directories pre-defined in the `config.R`,

``` r
"content/config.R" %>%
    here() %>%
    source()
```

## Data

``` r
bamfiles <- list.files(file.path(proc_dir, "stage3"),
                       pattern = "*.bam$",
                       full.names = TRUE,
                       recursive = TRUE)
mic.maf <- readRDS(mafobj)

# --- format manifest using JG's code
# --- found here: 
# --- https://gitlab.oit.duke.edu/mic-course/2024-mic/-/blob/main/content/bioinformatics/setup/generate_metadata_table.Rmd?ref_type=heads&plain=1#L62
m <- read.csv(manifest.path, header = TRUE) %>%
  separate(SampleName,
           into = c("Subject_ID", "Body_Site"),
           sep = "_",
           remove = FALSE) %>%
  mutate(Tumor = Body_Site != "GL")

m.slt <- m %>%
  filter(Tumor) %>%
  select(sample = Run,
         Body_Site)


# --- create bamdf
data.frame(path = bamfiles) %>%
  separate(path,
           into = c(NA, NA, NA, NA, NA,
                    NA, NA, "library", NA),
           sep = "/",
           remove = FALSE) -> bamdf

# --- create alt genes df
mic.maf@data %>%
  select(Tumor_Sample_Barcode,
         Hugo_Symbol,
         Variant_Classification,
         Chromosome,
         Start_Position,
         Reference_Allele,
         Allele) %>%
  mutate(variantID = paste0(Chromosome, ":",
                            Start_Position, ":",
                            Reference_Allele, "/",
                            Allele)) -> altgenes
```

# Gviz pileup

The official `Gviz` [User
Guide](https://bioconductor.org/packages/release/bioc/vignettes/Gviz/inst/doc/Gviz.html)
can be found here and includes all of the nitty gritty details about the
different customizations offered by the `Gviz` package.

## BAP1

Look at BAP1 mutation (chr3:52407288:G/A) for one sample: SRR12415473

``` r
# --- settings
options(ucscChromosomeNames = FALSE)

# --- sTrack using BSgenome hg38
sTrack <- Gviz::SequenceTrack(Hsapiens)

# --- pull variant information for one sample and one gene, BAP1
var.slt <- altgenes %>%
  filter(Tumor_Sample_Barcode == "SRR12415473",
         Hugo_Symbol == "BAP1")

# --- pull chromsome
mychr <- var.slt %>%
  pull(Chromosome) %>%
  unique()

# --- pull start position of variant
start <- var.slt %>%
  pull(Start_Position) %>%
  unique()

w <- 0 # how many bp to highlight around variant region of interest
bp <- 25
myp0 <- (start - bp) # plot +-5 bp around variant region
myp1 <- (start + bp)

# --- subset bamdf by sample ID
id <- var.slt %>%
  pull(Tumor_Sample_Barcode)

bam <- bamdf$path[bamdf$library == id]

# --- plot alignment track which is the actual pileup
alTrack <- Gviz::AlignmentsTrack(bam,
                                 alpha.reads = 0.2,
                                 isPaired = TRUE,
                                 showIndels = TRUE,
                                 chromosome = mychr,
                                 col.insertion = "purple",
                                 col.deletion = "orange")

# --- tell Gviz to highlight the variant region to make it easier to visualize
hTrack <- Gviz::HighlightTrack(trackList = list(alTrack, sTrack),
                               start = start,
                               width = w,
                               chromosome = mychr)

# --- add a genome axis
gTrack <- GenomeAxisTrack()

# --- plot all of the tracks together to create the final plot!
plotTracks(c(sTrack, gTrack, hTrack),
           chromosome = mychr,
           from = myp0,
           to = myp1,
           type = c("coverage", "pileup"),
           cex = 0.5,
           min.height = 0.5)
```

<img src="gviz_files/figure-markdown_github/unnamed-chunk-4-1.png" width="80%" style="display: block; margin: auto;" />

# Zoom out from the BAP1 mutation

``` r
bp <- 1000
myp0 <- (start - bp) # plot +-5 bp around variant region
myp1 <- (start + bp)

#  Add ideogram
itrack <- IdeogramTrack(genome = metadata(Hsapiens)["genome"][[1]], 
                        chromosome = mychr)

# --- plot all of the tracks together to create the final plot!
plotTracks(c(itrack, sTrack, gTrack, hTrack),
           chromosome = mychr,
           from = myp0,
           to = myp1,
           type = c("coverage", "pileup"),
           cex = 0.5,
           min.height = 0.5)
```

<img src="gviz_files/figure-markdown_github/unnamed-chunk-5-1.png" width="80%" style="display: block; margin: auto;" />

# Session information

``` r
sessionInfo()

print(glue("Start Time: ",startdt))
print(glue("End Time: ",date()))
```

    ## R version 4.4.0 (2024-04-24)
    ## Platform: x86_64-pc-linux-gnu
    ## Running under: Ubuntu 20.04.6 LTS
    ## 
    ## Matrix products: default
    ## BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0 
    ## LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.9.0
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## time zone: America/New_York
    ## tzcode source: system (glibc)
    ## 
    ## attached base packages:
    ## [1] grid      stats4    stats     graphics  grDevices utils     datasets 
    ## [8] methods   base     
    ## 
    ## other attached packages:
    ##  [1] fs_1.6.4                          tidyr_1.3.1                      
    ##  [3] stringr_1.5.1                     here_1.0.1                       
    ##  [5] dplyr_1.1.4                       glue_1.7.0                       
    ##  [7] maftools_2.20.0                   BSgenome.Hsapiens.UCSC.hg38_1.4.5
    ##  [9] BSgenome_1.72.0                   BiocIO_1.14.0                    
    ## [11] GenomicAlignments_1.40.0          Rsamtools_2.20.0                 
    ## [13] Biostrings_2.72.0                 XVector_0.44.0                   
    ## [15] SummarizedExperiment_1.34.0       Biobase_2.64.0                   
    ## [17] MatrixGenerics_1.16.0             matrixStats_1.3.0                
    ## [19] rtracklayer_1.64.0                RColorBrewer_1.1-3               
    ## [21] data.table_1.15.4                 Gviz_1.48.0                      
    ## [23] GenomicRanges_1.56.0              GenomeInfoDb_1.40.0              
    ## [25] IRanges_2.38.0                    S4Vectors_0.42.0                 
    ## [27] BiocGenerics_0.50.0              
    ## 
    ## loaded via a namespace (and not attached):
    ##   [1] rstudioapi_0.16.0        jsonlite_1.8.8           magrittr_2.0.3          
    ##   [4] GenomicFeatures_1.56.0   rmarkdown_2.26           zlibbioc_1.50.0         
    ##   [7] vctrs_0.6.5              memoise_2.0.1            RCurl_1.98-1.14         
    ##  [10] base64enc_0.1-3          htmltools_0.5.8.1        S4Arrays_1.4.0          
    ##  [13] progress_1.2.3           curl_5.2.1               SparseArray_1.4.3       
    ##  [16] Formula_1.2-5            htmlwidgets_1.6.4        httr2_1.0.1             
    ##  [19] cachem_1.0.8             lifecycle_1.0.4          pkgconfig_2.0.3         
    ##  [22] Matrix_1.7-0             R6_2.5.1                 fastmap_1.1.1           
    ##  [25] GenomeInfoDbData_1.2.12  digest_0.6.35            colorspace_2.1-0        
    ##  [28] AnnotationDbi_1.66.0     rprojroot_2.0.4          Hmisc_5.1-2             
    ##  [31] RSQLite_2.3.6            filelock_1.0.3           fansi_1.0.6             
    ##  [34] httr_1.4.7               abind_1.4-5              compiler_4.4.0          
    ##  [37] withr_3.0.0              bit64_4.0.5              htmlTable_2.4.2         
    ##  [40] backports_1.4.1          BiocParallel_1.38.0      DBI_1.2.2               
    ##  [43] highr_0.10               biomaRt_2.60.0           rappdirs_0.3.3          
    ##  [46] DelayedArray_0.30.1      rjson_0.2.21             DNAcopy_1.78.0          
    ##  [49] tools_4.4.0              foreign_0.8-86           nnet_7.3-19             
    ##  [52] restfulr_0.0.15          checkmate_2.3.1          cluster_2.1.6           
    ##  [55] generics_0.1.3           gtable_0.3.5             ensembldb_2.28.0        
    ##  [58] hms_1.1.3                xml2_1.3.6               utf8_1.2.4              
    ##  [61] pillar_1.9.0             splines_4.4.0            BiocFileCache_2.12.0    
    ##  [64] lattice_0.22-6           survival_3.6-4           bit_4.0.5               
    ##  [67] deldir_2.0-4             biovizBase_1.52.0        tidyselect_1.2.1        
    ##  [70] knitr_1.46               gridExtra_2.3            ProtGenerics_1.36.0     
    ##  [73] xfun_0.43                stringi_1.8.4            UCSC.utils_1.0.0        
    ##  [76] lazyeval_0.2.2           yaml_2.3.8               evaluate_0.23           
    ##  [79] codetools_0.2-20         interp_1.1-6             tibble_3.2.1            
    ##  [82] cli_3.6.2                rpart_4.1.23             munsell_0.5.1           
    ##  [85] dichromat_2.0-0.1        Rcpp_1.0.12              dbplyr_2.5.0            
    ##  [88] png_0.1-8                XML_3.99-0.16.1          parallel_4.4.0          
    ##  [91] ggplot2_3.5.1            blob_1.2.4               prettyunits_1.2.0       
    ##  [94] latticeExtra_0.6-30      jpeg_0.1-10              AnnotationFilter_1.28.0 
    ##  [97] bitops_1.0-7             VariantAnnotation_1.50.0 scales_1.3.0            
    ## [100] purrr_1.0.2              crayon_1.5.2             rlang_1.1.3             
    ## [103] KEGGREST_1.44.0         
    ## Start Time: Thu May 23 06:56:03 2024
    ## End Time: Thu May 23 06:56:41 2024
