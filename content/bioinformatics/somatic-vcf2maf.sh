#!/bin/sh

#'Tutorial: Variant annotation (VEP/maftools)'

export wd=/work/${USER}/mic2024_proc
export groupdir=/hpc/group/mic-2024
export veppath=/opt/vep/src/ensembl-vep
export vepcache=${groupdir}/references/vepcache
export fasta=${groupdir}/references/GATK/hg38/v0/Homo_sapiens_assembly38.fasta
export invcf=${groupdir}/downsampled_proc/somatic/somatic_filtered.vcf.gz
export outdir=${wd}/vcf2maf
export tumor_library=SRR12415554 
export normal_library=SRR12415556 

mkdir -p ${outdir}

sif=/opt/apps/containers/community/mic-2024/2024-mic-vep-simage_latest.sif

singularity exec \
  --no-home \
  --app samtools \
  --bind ${wd}:${wd}:rw \
  --bind ${groupdir}:${groupdir}:ro \
  ${sif} bcftools view \
  --apply-filters 'PASS' \
  --output ${outdir}/somatic_filtered_PASS.vcf \
  ${invcf}
  
singularity exec \
  --no-home \
  --app samtools \
  --bind ${wd}:${wd}:rw \
  --bind ${groupdir}:${groupdir}:ro \
  ${sif} perl /scripts/vcf2maf_mod.pl \
  --input-vcf ${outdir}/somatic_filtered_PASS.vcf \
  --output-maf ${outdir}/somatic_vep.maf \
  --tumor-id ${tumor_library} \
  --normal-id ${normal_library} \
  --ref-fasta ${fasta} \
  --ncbi-build GRCh38 \
  --species homo_sapiens \
  --vep-overwrite \
  --vep-path ${veppath} \
  --vep-data ${vepcache} \
  --cache-version 110

# NOTE! It's OK: you will see the following warning message:
# "WARNING: Unrecognized biotype "<biotype>". Assigning lowest priority!"
# 
# This just indicates that newer versions of Ensembl VEP (which is called
# internally by vcf2maf) contains new biotypes/effects that have not yet been 
# included in vcf2maf.
