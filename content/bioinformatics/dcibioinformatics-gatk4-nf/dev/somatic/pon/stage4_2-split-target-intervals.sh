# split targets.interval_list to run with stage4_2
# --- you can run in an srun interactive session since this runs very quickly
srun \
  --partition=chsi-high \
  --account=mic-2024 \
  --nodes=1 \
  --ntasks-per-node=1 \
  --cpus-per-task=20 \
  --pty bash -i

# --- set paths (for running on HARDAC)
wd=/hpc/group/mic-2024/references/Captures
sif=/opt/apps/containers/community/mic-2024/2024-mic-singularity-image_latest.sif
ref=/hpc/group/mic-2024/references/GATK/hg38
proc=/work/mic-2024-gatk/captures
tmp=/scratch/

singularity shell \
  --app gatk \
  --bind ${wd}:${wd}:rw \
  --bind ${ref}:${ref}:ro \
  --bind ${tmp}:${tmp}:rw \
  --bind ${proc}:${proc}:rw \
  ${sif} \
  bash 

wd=/work/mic-2024-gatk
captures=/hpc/group/mic-2024/references/Captures
ref=/hpc/group/mic-2024/references/GATK/hg38
tmp=/scratch/
subintervals=${wd}/captures/V6_sub_intervals
intervals=${captures}/S07604514_Padded.interval_list 
genome=${ref}/v0/Homo_sapiens_assembly38.fasta

gatk --java-options "-Xmx16G -Xms8G" SplitIntervals \
  --reference ${genome} \
  --intervals ${intervals} \
  --scatter-count 10 \
  --subdivision-mode BALANCING_WITHOUT_INTERVAL_SUBDIVISION_WITH_OVERFLOW \
  --output ${subintervals} \
  --tmp-dir ${tmp}