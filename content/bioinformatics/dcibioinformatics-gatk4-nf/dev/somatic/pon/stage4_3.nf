
workflow {

  Channel 
      .fromPath( params.samplesheet_stg4_3, checkIfExists: true )
      .splitCsv( header: true )
      .map { row -> [row.intervallist,
                     file(row.ponpath)] }
      .groupTuple( by: 0 )
      .set { subpon_ch }

  Channel 
      .fromPath( params.tmpdir, checkIfExists: true )
      .first()
      .set { tmp_ch }

  mergepons( subpon_ch,
             tmp_ch )
  sortpon( mergepons.out.mergedpon,
           tmp_ch )

}


process mergepons {

  containerOptions "--app gatk"
  publishDir "${params.outdir}/${params.stg4_3}/MergePON", mode: "copy"
  tag "Merging sub PONs"
  label 'lowresource'

  input:
  tuple val(kit), path(subpons, stageAs: "subint??/*")
  path tmp

  output:
  path "*"
  path "merged_pon.vcf.gz", emit: mergedpon

  script:
  def vcflist = subpons.collect{"--input $it"}.join(' ')

  """
  gatk --java-options ${params.java_options} GatherVcfsCloud \
  ${vcflist} \
  --output merged_pon.vcf.gz \
  --create-output-variant-index true \
  --tmp-dir ${tmp}
  """

}


process sortpon {

  containerOptions "--app bcftools"
  publishDir "${params.outdir}/${params.stg4_3}/SortPON", mode: "copy"
  tag "Sorting merged PON"
  label 'lowresource'

  input:
  path mergedpon
  path tmp 

  output:
  path "*"

  script:
  """
  bcftools sort \
    --output sorted_merged_normals_pon.vcf.gz \
    --output-type z \
    --temp-dir ${tmp} \
    ${mergedpon}

  bcftools index \
    --tbi \
    --output sorted_merged_normals_pon.vcf.gz.tbi \
    sorted_merged_normals_pon.vcf.gz

  """

}

