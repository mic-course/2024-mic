#!/bin/bash
#SBATCH --job-name=gatk-stage4_2-mic
#SBATCH --account=mic-2024
#SBATCH --mail-user=lwr11@duke.edu
#SBATCH --mail-type=END,FAIL
#SBATCH --output=./logs/gatk-stage4_2-mic-%j

nextflow=/hpc/home/lwr11/software/nextflow/23.10.1/nextflow
tm=$(date '+%Y-%m-%d_%H-%M-%S')

${nextflow} run -c gatk_pon_SLURM.config stage4_2.nf -with-report stg4_2-exec-${tm}.html -with-timeline stg4_2-timeline-${tm}.html
