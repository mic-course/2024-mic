
workflow {

  Channel 
      .fromPath( params.samplesheet_stg4_1, checkIfExists: true )
      .splitCsv( header: true )
      .map { row -> [row.library, 
                     file(row.bams), 
                     file(row.bais)] }
      .set { recal_bam_ch }
  
  Channel 
      .fromPath( params.gatk_genome_dir, checkIfExists: true )
      .first()
      .set { genome_ch }

  Channel 
      .fromPath( params.tmpdir, checkIfExists: true )
      .first()
      .set { tmp_ch }

  mutect2Normals( recal_bam_ch,
                  genome_ch,
                  tmp_ch )

}


process mutect2Normals {

  containerOptions "--app gatk"
  publishDir "${params.outdir}/${params.stg4_1}/mutect2Normals/${uid}", mode: "copy"
  tag "Mutect2 on $uid"
  label 'lowresource'

  input:
  tuple val(uid), path(recalbam), path(recalbai)
  path ref 
  path tmp 

  output:
  tuple val(uid), path("*")

  script:
  """
  gatk --java-options ${params.java_options} Mutect2 \
    --reference ${ref}/${params.gatk_genome} \
    --input ${recalbam} \
    --max-mnp-distance 0 \
    --output normal_pon.vcf.gz \
    --tmp-dir ${tmp}
  """

}
