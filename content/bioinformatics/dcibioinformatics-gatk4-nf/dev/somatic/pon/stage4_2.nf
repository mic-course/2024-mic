
workflow {

  Channel 
      .fromPath( params.subintervals_stg4_2, checkIfExists: true )
      .splitCsv( header: true )
      .map { row -> row.subIDs }
      .set { subintid_ch }

  Channel 
      .fromPath( params.subintervals_stg4_2, checkIfExists: true )
      .splitCsv( header: true )
      .map { row -> file(row.subs) }
      .set { subint_ch }

  Channel 
      .fromPath( params.samplemap_stg4_2, checkIfExists: true )
      .first()
      .set { map_ch }

  Channel 
      .fromPath( params.gatk_genome_dir, checkIfExists: true )
      .first()
      .set { genome_ch }

  Channel 
      .fromPath( params.tmpdir, checkIfExists: true )
      .first()
      .set { tmp_ch }

  subintid_ch
      .merge(subint_ch)
      .set { subs_ch }	

  genomicsdbnormals( subs_ch,
                     map_ch,
                     genome_ch,
                     tmp_ch )
  createpon( genomicsdbnormals.out.subdb,
             genome_ch,
             tmp_ch )

}


process genomicsdbnormals {

  containerOptions "--app gatk"
  publishDir "${params.outdir}/${params.stg4_2}/GenomicsDB_Normals", mode: "copy"
  tag "GenomicsDBImport"
  label 'highresource'

  input:
  each sub
  path map
  path ref
  path tmp 

  output:
  path("subint*", type: "dir", emit: "subdb")
  path ".command.log"

  script:
  """
  gatk --java-options "-Xmx96G -Xms96G" GenomicsDBImport \
  --reference ${ref}/${params.gatk_genome} \
  --intervals ${sub[1]} \
  --genomicsdb-workspace-path ${sub[0]} \
  --overwrite-existing-genomicsdb-workspace true \
  --genomicsdb-shared-posixfs-optimizations true \
  --bypass-feature-reader true \
  --merge-input-intervals true \
  --sample-name-map ${map} \
  --tmp-dir ${tmp}
  """

}

process createpon {

  containerOptions "--app gatk"
  publishDir "${params.outdir}/${params.stg4_2}/CreatePON/${subdb}", mode: "copy"
  tag "CreatePON"
  label 'highresource'

  input:
  path subdb
  path ref 
  path tmp 

  output:
  path "pon.vcf.*"
  path ".command.log"

  script:
  """
  gatk --java-options ${params.java_options} CreateSomaticPanelOfNormals \
  --reference ${ref}/${params.gatk_genome} \
  --germline-resource ${ref}/${params.afonly} \
  --variant gendb://${subdb} \
  --genomicsdb-shared-posixfs-optimizations true \
  --output pon.vcf.gz \
  --tmp-dir ${tmp}
  """

}
