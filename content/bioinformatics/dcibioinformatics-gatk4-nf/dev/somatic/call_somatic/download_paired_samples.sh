ssh hardac-xfer.genome.duke.edu

tmux new -s s3
aws=/home/lwr11/local/bin/aws
BUCKET=dh-dusom-dcbsm-ander118
PROFILE=anders

# download fastqs for tumor sample that matches
# DCBSM-NC010-10098-F04-01A-01D -> DCBSM-NC010-10098-M07-02A-01D

# b1
$aws --profile ${PROFILE} \
  s3 ls s3://${BUCKET}/BCBrM/RawData/Swearingen_7962_221115B7/

# b2
$aws --profile ${PROFILE} \
  s3 ls s3://${BUCKET}/BCBrM/RawData/Swearingen_7962_221118A6/

# --- download b1 files
$aws s3 cp s3://${BUCKET}/BCBrM/RawData/Swearingen_7962_221115B7/ /data/dcibioinformatics/Anders/Proc/DNA/gatk_test/fastqs \
  --exclude "*" \
  --include "DCBSM-NC010-10098-M07-02A-01D_*" \
  --profile ${PROFILE} \
  --recursive \
  --dryrun

$aws s3 cp s3://${BUCKET}/BCBrM/RawData/Swearingen_7962_221115B7/ /data/dcibioinformatics/Anders/Proc/DNA/gatk_test/fastqs \
  --exclude "*" \
  --include "DCBSM-NC010-10098-M07-02A-01D_*" \
  --profile ${PROFILE} \
  --recursive

# --- download b2 files
$aws s3 cp s3://${BUCKET}/BCBrM/RawData/Swearingen_7962_221118A6/ /data/dcibioinformatics/Anders/Proc/DNA/gatk_test/fastqs \
  --exclude "*" \
  --include "DCBSM-NC010-10098-M07-02A-01D_*" \
  --profile ${PROFILE} \
  --recursive \
  --dryrun

$aws s3 cp s3://${BUCKET}/BCBrM/RawData/Swearingen_7962_221118A6/ /data/dcibioinformatics/Anders/Proc/DNA/gatk_test/fastqs \
  --exclude "*" \
  --include "DCBSM-NC010-10098-M07-02A-01D_*" \
  --profile ${PROFILE} \
  --recursive