
workflow {

  Channel
    .fromPath( params.samplesheet_stg5_up, checkIfExists: true )
    .splitCsv( header: true )
    .map { row -> [row.sampleid, 
                   file(row.tumorbam), 
                   file(row.tumorbai)] }
    .set { recal_bam_ch }

  Channel 
    .fromPath( params.gatk_genome_dir, checkIfExists: true )
    .first()
    .set { genome_ch }

  Channel
    .fromPath( params.anno_dir, checkIfExists: true )
    .first()
    .set { anno_ch }

  Channel 
    .fromPath( params.captures_dir, checkIfExists: true )
    .first()
    .set { captures_ch }

  Channel 
    .fromPath( params.tmpdir, checkIfExists: true )
    .first()
    .set { tmp_ch }

  Channel
    .fromPath( params.pon_dir, checkIfExists: true )
    .first()
    .set { pon_ch }

  mutect2_unpaired( recal_bam_ch,
                    genome_ch,
                    captures_ch,
                    tmp_ch,
                    pon_ch )

  J1 = mutect2_unpaired.out.somvcf.join(mutect2_unpaired.out.somvcftbi, by: 0)
  J2 = J1.join(mutect2_unpaired.out.somstats, by: 0)

  learn_read_orientation_model( mutect2_unpaired.out.f1r2,
                                tmp_ch )
  pileup_summary_tumor( recal_bam_ch,
                        genome_ch,
                        anno_ch,
                        tmp_ch )
  calculate_contamination( pileup_summary_tumor.out.pileupt,
                           tmp_ch )

  J3 = J2.join(learn_read_orientation_model.out.romodel, by: 0)
  J4 = J3.join(calculate_contamination.out.contamtab, by: 0)

  mutect2_filter( J4,
                  genome_ch,
                  tmp_ch )

}

process mutect2_unpaired {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg5}/unpaired/mutect2_unpaired/${uid}", mode: "copy"
  tag "Mutect2 on $uid"
  // label 'highresource'

  input:
  tuple val(uid), path(tumorbam), path(tumorbai)
  path ref
  path captures
  path tmp
  path pondir

  output:
  tuple val(uid), path("f1r2.tar.gz"), emit: f1r2
  tuple val(uid), path("somatic.vcf.gz"), emit: somvcf
  tuple val(uid), path("somatic.vcf.gz.tbi"), emit: somvcftbi
  tuple val(uid), path("somatic.vcf.gz.stats"), emit: somstats
  tuple val(uid), path("*")

  script:
  """
  gatk --java-options '${params.java_options}' Mutect2 \
    --native-pair-hmm-threads ${params.threads} \
    --reference ${ref}/${params.gatk_genome} \
    --input ${tumorbam} \
    --panel-of-normals ${pondir}/${params.pon} \
    --germline-resource ${ref}/${params.afonly} \
    --intervals ${captures}/${params.targets} \
    --f1r2-tar-gz f1r2.tar.gz \
    --output somatic.vcf.gz \
    --bam-output somatic.bam \
    --tmp-dir ${tmp}
  """

}

process learn_read_orientation_model {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg5}/unpaired/learn_read_orientation_model/${uid}", mode: "copy"
  tag "LearnReadOrientationModel on $uid"
  // label 'lowresource'

  input:
  tuple val(uid), path(f1r2)
  path tmp

  output:
  tuple val(uid), path("read-orientation-model.tar.gz"), emit: romodel
  tuple val(uid), path("*")

  script:
  """
  gatk --java-options '${params.java_options}' LearnReadOrientationModel \
    --input ${f1r2} \
    --output read-orientation-model.tar.gz \
    --tmp-dir ${tmp}
  """

}

process pileup_summary_tumor {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg5}/unpaired/pileup_summary_tumor/${uid}", mode: "copy"
  tag "GetPileupSummaries on $uid"
  // label 'highresource'

  input:
  tuple val(uid), path(tumorbam), path(tumorbai)
  path ref
  path anno
  path tmp

  output:
  tuple val(uid), path("pileup_summary_tumor.table"), emit: pileupt
  tuple val(uid), path("*")

  script:
  """
  gatk --java-options '${params.java_options}' GetPileupSummaries \
    --input ${tumorbam} \
    --variant ${anno}/${params.commonvcf} \
    --intervals ${anno}/${params.commonvcf} \
    --output pileup_summary_tumor.table \
    --tmp-dir ${tmp}
  """

}

process calculate_contamination {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg5}/unpaired/calculate_contamination/${uid}", mode: "copy"
  tag "CalculateContamination on $uid"
  // label 'highresource'

  input:
  tuple val(uid), path(pileupt)
  path tmp

  output:
  tuple val(uid), path("contamination_unpaired.table"), path("segments_unpaired.tsv"), emit: contamtab
  tuple val(uid), path("*")

  script:
  """
  gatk --java-options '${params.java_options}' CalculateContamination \
    --input ${pileupt} \
    --output contamination_unpaired.table \
    --tumor-segmentation segments_unpaired.tsv \
    --tmp-dir ${tmp}
  """

}

process mutect2_filter {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg5}/unpaired/mutect2_filter/${uid}", mode: "copy"
  tag "FilterMutectCalls on $uid"
  // label 'lowresource'

  input:
  tuple val(uid), path(somvcf), path(somvcftbi), path(somstats), path(romodel), path(contamtab), path(segtab)
  path ref
  path tmp

  output:
  tuple val(uid), path("*")

  script:
  """
  gatk --java-options '${params.java_options}' FilterMutectCalls \
    --reference ${ref}/${params.gatk_genome} \
    --contamination-table ${contamtab} \
    --orientation-bias-artifact-priors ${romodel} \
    --variant ${somvcf} \
    --tumor-segmentation ${segtab} \
    --output somatic_filtered.vcf.gz \
    --tmp-dir ${tmp}
  """

}
