
workflow {

  Channel
    .fromPath( params.samplesheet_stg5_p, checkIfExists: true )
    .splitCsv( header: true )
    .map { row -> [row.sampleid, 
                   file(row.tumorbam), 
                   file(row.tumorbai),
                   file(row.normalid),
                   file(row.normalbam),
                   file(row.normalbai)] }
    .set { recal_bam_ch }

  Channel 
    .fromPath( params.gatk_genome_dir, checkIfExists: true )
    .first()
    .set { genome_ch }

  Channel 
    .fromPath( params.captures_dir, checkIfExists: true )
    .first()
    .set { captures_ch }

  Channel 
    .fromPath( params.tmpdir, checkIfExists: true )
    .first()
    .set { tmp_ch }

  Channel
    .fromPath( params.pon_dir, checkIfExists: true )
    .first()
    .set { pon_ch }

  mutect2_paired( recal_bam_ch,
                  genome_ch,
                  captures_ch,
                  tmp_ch,
                  pon_ch )

  J1 = mutect2_paired.out.somvcf.join(mutect2_paired.out.somvcftbi, by: 0)
  J2 = J1.join(mutect2_paired.out.somstats, by: 0)

  learn_read_orientation_model( mutect2_paired.out.f1r2,
                                tmp_ch )
  pileup_summary_tumor( recal_bam_ch,
                        genome_ch,
                        tmp_ch )
  pileup_summary_normal( recal_bam_ch,
                         genome_ch,
                         tmp_ch )

  J3 = pileup_summary_tumor.out.pileupt.join(pileup_summary_normal.out.pileupn, by: 0)

  calculate_contamination( J3,
                           tmp_ch )

  J4 = J2.join(learn_read_orientation_model.out.romodel, by: 0)
  J5 = J4.join(calculate_contamination.out.contamtab, by: 0)

  mutect2_filter( J5,
                  genome_ch,
                  tmp_ch )

}

process mutect2_paired {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg5}/paired/mutect2_paired/${uid}", mode: "copy"
  tag "Mutect2 on $uid"
  label 'lowresource'

  input:
  tuple val(uid), path(tumorbam, stageAs: "tumor.bam"), path(tumorbai, stageAs: "tumor.bai"), path(normalid), path(normalbam, stageAs: "normal.bam"), path(normalbai, stageAs: "normal.bai")
  path ref
  path captures
  path tmp
  path pondir

  output:
  tuple val(uid), path("f1r2.tar.gz"), emit: f1r2
  tuple val(uid), path("somatic.vcf.gz"), emit: somvcf
  tuple val(uid), path("somatic.vcf.gz.tbi"), emit: somvcftbi
  tuple val(uid), path("somatic.vcf.gz.stats"), emit: somstats
  tuple val(uid), path("*")

  script:
  """
  gatk --java-options '${params.java_options}' Mutect2 \
    --native-pair-hmm-threads ${params.threads} \
    --reference ${ref}/${params.gatk_genome} \
    --input ${tumorbam} \
    --input ${normalbam} \
    --normal-sample ${normalid} \
    --panel-of-normals ${pondir}/${params.pon} \
    --germline-resource ${ref}/${params.afonly} \
    --intervals ${captures}/${params.targets} \
    --f1r2-tar-gz f1r2.tar.gz \
    --output somatic.vcf.gz \
    --bam-output somatic.bam \
    --tmp-dir ${tmp}
  """

}

process learn_read_orientation_model {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg5}/paired/learn_read_orientation_model/${uid}", mode: "copy"
  tag "LearnReadOrientationModel on $uid"
  label 'lowresource'

  input:
  tuple val(uid), path(f1r2)
  path tmp

  output:
  tuple val(uid), path("read-orientation-model.tar.gz"), emit: romodel
  tuple val(uid), path("*")

  script:
  """
  gatk --java-options '${params.java_options}' LearnReadOrientationModel \
    --input ${f1r2} \
    --output read-orientation-model.tar.gz \
    --tmp-dir ${tmp}
  """

}

process pileup_summary_tumor {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg5}/paired/pileup_summary_tumor/${uid}", mode: "copy"
  tag "GetPileupSummaries on $uid"
  label 'lowresource'

  input:
  tuple val(uid), path(tumorbam, stageAs: "tumor.bam"), path(tumorbai, stageAs: "tumor.bai"), path(normalid), path(normalbam, stageAs: "normal.bam"), path(normalbai, stageAs: "normal.bai")
  path ref
  path tmp

  output:
  tuple val(uid), path("pileup_summary_tumor.table"), emit: pileupt
  tuple val(uid), path("*")

  script:
  """
  gatk --java-options '${params.java_options}' GetPileupSummaries \
    --input ${tumorbam} \
    --variant ${ref}/${params.commonvcf} \
    --intervals ${ref}/${params.commonvcf} \
    --output pileup_summary_tumor.table \
    --tmp-dir ${tmp}
  """

}

process pileup_summary_normal {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg5}/paired/pileup_summary_normal/${uid}", mode: "copy"
  tag "GetPileupSummaries on $uid"
  label 'lowresource'

  input:
  tuple val(uid), path(tumorbam, stageAs: "tumor.bam"), path(tumorbai, stageAs: "tumor.bai"), path(normalid), path(normalbam, stageAs: "normal.bam"), path(normalbai, stageAs: "normal.bai")
  path ref
  path tmp

  output:
  tuple val(uid), path("pileup_summary_normal.table"), emit: pileupn
  tuple val(uid), path("*")

  script:
  """
  gatk --java-options '${params.java_options}' GetPileupSummaries \
    --input ${normalbam} \
    --variant ${ref}/${params.commonvcf} \
    --intervals ${ref}/${params.commonvcf} \
    --output pileup_summary_normal.table \
    --tmp-dir ${tmp}
  """

}

process calculate_contamination {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg5}/paired/calculate_contamination/${uid}", mode: "copy"
  tag "CalculateContamination on $uid"
  label 'lowresource'

  input:
  tuple val(uid), path(pileupt), path(pileupn)
  path tmp

  output:
  tuple val(uid), path("contamination_paired.table"), path("segments_paired.tsv"), emit: contamtab
  tuple val(uid), path("*")

  script:
  """
  gatk --java-options '${params.java_options}' CalculateContamination \
    --input ${pileupt} \
    --matched-normal ${pileupn} \
    --output contamination_paired.table \
    --tumor-segmentation segments_paired.tsv \
    --tmp-dir ${tmp}
  """

}

process mutect2_filter {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg5}/paired/mutect2_filter/${uid}", mode: "copy"
  tag "FilterMutectCalls on $uid"
  label 'lowresource'

  input:
  tuple val(uid), path(somvcf), path(somvcftbi), path(somstats), path(romodel), path(contamtab), path(segtab)
  path ref
  path tmp

  output:
  tuple val(uid), path("*")

  script:
  """
  gatk --java-options '${params.java_options}' FilterMutectCalls \
    --reference ${ref}/${params.gatk_genome} \
    --contamination-table ${contamtab} \
    --orientation-bias-artifact-priors ${romodel} \
    --variant ${somvcf} \
    --output somatic_filtered.vcf.gz \
    --tumor-segmentation ${segtab} \
    --tmp-dir ${tmp}
  """

}
