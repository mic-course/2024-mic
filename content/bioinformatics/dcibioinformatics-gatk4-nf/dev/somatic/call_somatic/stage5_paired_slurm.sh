#!/bin/bash
#SBATCH --job-name=gatk-stage5-paired-mic
#SBATCH --account=mic-2024
#SBATCH --mail-user=lwr11@duke.edu
#SBATCH --mail-type=END,FAIL
#SBATCH --output=./logs/gatk-stage5-paired-mic-%j

nextflow=/hpc/home/lwr11/software/nextflow/23.10.1/nextflow
tm=$(date '+%Y-%m-%d_%H-%M-%S')

${nextflow} run -c gatk_somatic_SLURM.config stage5_paired.nf -with-report stg5_paired-exec-${tm}.html -with-timeline stg5_paired-timeline-${tm}.html
