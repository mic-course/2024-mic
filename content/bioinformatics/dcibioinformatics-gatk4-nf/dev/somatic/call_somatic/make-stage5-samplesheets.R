srun \
  --partition=chsi-high \
  --account=mic-2024 \
  --nodes=1 \
  --ntasks-per-node=1 \
  --cpus-per-task=20 \
  --pty bash -i

sif=/opt/apps/containers/community/mic-2024/dcibioinformaticsR-v2.2.sif
wd=/work/mic-2024-gatk
raw=/hpc/group/mic-2024

singularity exec \
  --containall \
  --no-home \
  --bind ${wd}:${wd}:rw \
  --bind ${raw}:${raw}:ro \
  ${sif} \
  R

library(tidyverse)
library(openxlsx)

wd <- "/work/mic-2024-gatk"
raw <- "/hpc/group/mic-2024/rawdata"
manifest.path <- file.path(raw, "sra_metadata.csv")

m <- read.csv(manifest.path, header = TRUE) %>%
  separate(SampleName,
           into = c("Subject_ID", "Body_Site"),
           sep = "_",
           remove = FALSE) %>%
  mutate(Tumor = Body_Site != "GL")

sam <- m %>%
  select(Run,
         Subject_ID,
         Tumor,
         Body_Site)

paired <- FALSE
unpaired <- TRUE

if (paired) {

  ############################################
  # SAMPLES WITH MATCHED NORMAL
  ############################################

  # --- which patients have tumor-normal pairs?
  # length(unique(sam$Subject_ID)) # 22
  sam %>% 
    group_by(Subject_ID) %>% 
    summarize(catTumor = paste0(unique(Tumor), collapse = ","),
              catTypes = paste0(unique(Body_Site), collapse = ","),
              nLibs = n()) %>% 
    filter(grepl("FALSE", catTumor) &
            nLibs > 1) %>% 
    pull(Subject_ID) %>% 
    unique() -> pairs

  # --- create simplified group classifications for each sample/patient
  sam.groups <- sam %>%
    filter(Subject_ID %in% pairs) %>% 
    mutate(Group.type = ifelse(Tumor, "Tumor", "Normal")) %>%
    unite("Patient.sample", 
          c("Subject_ID", "Body_Site"),
          sep = "_",
          remove = FALSE)

  # --- pivot tumor patient.samples
  tumor.df <- sam.groups %>% 
    filter(Tumor) %>% 
    select(Patient.sample, 
           Group.type,
           Patient.id = Subject_ID,
           sample.id = Run) %>% 
    pivot_wider(names_from = Group.type,
                values_from = sample.id)

  # --- pivot normal patient.samples and remove unique identifier column for merging
  normal.df <- sam.groups %>% 
    filter(!Tumor) %>% 
    select(Patient.sample, 
           Group.type,
           Patient.id = Subject_ID,
           sample.id = Run) %>% 
    pivot_wider(names_from = Group.type,
                values_from = sample.id) %>% 
    select(-Patient.sample) %>% 
    distinct()

  # --- merge tumor.df and normal.df by patient.id
  pairs.df <- tumor.df %>% 
    left_join(normal.df,
              by = "Patient.id")

  bamdir <- file.path(wd, "proc", "stage3") # path to recal.bam files (stage3)
  out <- file.path(wd, "proc/somatic/stage5/paired", "stage5_paired_samplesheet.csv") # output path for stage5_paired.samples

  data.frame(bam = list.files(bamdir, # pull bams
                              pattern = "recal.bam$",
                              full.names = TRUE,
                              recursive = TRUE),
             bai = list.files(bamdir, # pull bai for each bam
                              pattern = "recal.bai$",
                              full.names = TRUE,
                              recursive = TRUE)) %>%
    separate(bam,
             into = c(NA, NA, NA, NA, NA, NA,
                      "library", NA),
             remove = FALSE,
             sep = "/") %>%
    relocate(library) -> bam.df

  out.df.1 <- pairs.df %>% 
    left_join(bam.df,
              by = c("Tumor" = "library")) %>% 
    na.omit() %>% 
    rename(tumorbam = bam,
           tumorbai = bai)

  # dim(out.df.1) # 90 6

  out.df.2 <- out.df.1 %>% 
    left_join(bam.df,
              by = c("Normal" = "library")) %>% 
    rename(normalbam = bam,
          normalbai = bai) %>%
    select(sampleid = Patient.sample,
           patid = Patient.id,
           tumorid = Tumor,
           normalid = Normal,
           tumorbam,
           tumorbai,
           normalbam,
           normalbai)

  # dim(out.df.2) # 90 8

  write.csv(out.df.2, 
            row.names = FALSE, 
            quote = FALSE,
            out)

} else if (unpaired) {

  ############################################
  # SAMPLES WITHOUT MATCHED NORMAL
  ############################################
  sam %>% 
    group_by(Subject_ID) %>% 
    summarize(catTumor = paste0(unique(Tumor), collapse = ","),
              catTypes = paste0(unique(Body_Site), collapse = ","),
              nLibs = n()) %>% 
    filter(!grepl("FALSE", catTumor)) %>% 
    pull(Subject_ID) %>% 
    unique() -> unpaired

  # length(unpaired) # none

  sam.groups <- sam %>%
    filter(patient.id %in% unpaired) %>% 
    select(patient.id, 
           Sample.type, 
           Sample.processing, 
           sample.id) %>%
    mutate(Type = case_when(
      Sample.type == "Matched blood" ~ "Blood",
      Sample.type == "Matched EC tumor" ~ "EC",
      Sample.type == "BCBrM" ~ "BCBrM"
    ),
    Group.type = case_when(
      grepl("blood", Sample.type) ~ "Normal",
      grepl("EC", Sample.type) ~ "Tumor",
      grepl("BCBrM", Sample.type) ~ "Tumor"
    )) %>%
    unite("Patient.sample", 
          c("patient.id", "Sample.processing", "Type"),
          remove = FALSE) %>% 
    mutate(Patient.sample = paste0(Patient.sample, "_", row_number())) # *** each uid in the output dir will have a number at the end that corresponds to it's row number in this dataframe to make each line unique (some pts have multiples of one sample type) ***

  # dim(sam.groups) # 29  7

  # --- pivot tumor patient.samples
  tumor.df <- sam.groups %>% 
    filter(Group.type == "Tumor") %>% 
    select(Patient.sample, 
           Patient.id = patient.id,
           Group.type,
           sample.id) %>% 
    pivot_wider(names_from = Group.type,
                values_from = sample.id)

  # sam.groups %>% filter(Group.type == "Normal") # nothing

  bamdir <- file.path(wd, "Proc", "stage3") # path to recal.bam files (stage3)
  out <- file.path(wd, "Proc/somatic/stage5/unpaired", "stage5_unpaired_samplesheet.csv")

  data.frame(bam = list.files(bamdir,
                              pattern = "recal.bam$",
                              full.names = TRUE,
                              recursive = TRUE),
             bai = list.files(bamdir,
                              pattern = "recal.bai$",
                              full.names = TRUE,
                              recursive = TRUE)) %>%
    separate(bam,
             into = c(NA, NA, NA, NA, NA, NA,
                      NA, NA, NA, "library", NA),
             remove = FALSE,
             sep = "/") %>%
    relocate(library) -> bam.df

  # dim(bam.df)

  out.df.1 <- tumor.df %>% 
    left_join(bam.df,
              by = c("Tumor" = "library")) %>% 
    na.omit() %>% 
    rename(tumorbam = bam,
           tumorbai = bai) %>%
    select(sampleid = Patient.sample,
           library = Tumor,
           tumorbam,
           tumorbai)

  # dim(out.df.1)

  write.csv(out.df.1, 
            row.names = FALSE, 
            quote = FALSE,
            out)

}