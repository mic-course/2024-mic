# Installing Java and Nextflow on DCC
March 4 2024
Based on https://gitlab.oit.duke.edu/dcibioinformatics-internal/pipeline/dcibioinformatics-rnaseq-nf#installing-java-and-nextflow-on-hardac 

- Go to your home directory on DCC: /hpc/home/<netid> and create a software directory
- Under software/ create a java and nextflow directory

## Java

- cd to your java/ directory and download the most recent version of Java using wget.
- Java downloads: https://www.oracle.com/java/technologies/downloads/
- Make sure that you download the "x64 Compressed Archive (x64 Compressed Archive)" tar ball!
- Untar your file by running tar -xvf <*.tar.gz>

```
wget https://download.oracle.com/java/21/latest/jdk-21_linux-x64_bin.tar.gz
tar -xvf jdk-21_linux-x64_bin.tar.gz

```

- Add your Java installation to your .bashrc file by adding the following to your file:

```
nano ~/.bashrc

# add these two lines:
export JAVA_HOME=/hpc/home/<netid>/software/java/jdk-21.0.2
export PATH="/hpc/home/<netid>/software/:$PATH"


# save and close:
ctrl-x

# source changes:
source ~/.bashrc

echo $PATH
echo $JAVA_HOME
```

## Nextflow

cd to your nextflow/ directory and download the most recent version of nextflow by running 

```
wget -qO- https://get.nextflow.io | bash

```

Test your nextflow installation by running 

```
chmod +x nextflow
/hpc/home/<netid>/software/nextflow/nextflow -version
nextflow/nextflow -version
```



