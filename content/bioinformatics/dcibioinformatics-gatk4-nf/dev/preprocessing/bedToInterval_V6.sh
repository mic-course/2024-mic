# https://gatk.broadinstitute.org/hc/en-us/articles/360036726611-BedToIntervalList-Picard-

srun \
  --partition=chsi-high \
  --account=mic-2024 \
  --nodes=1 \
  --ntasks-per-node=1 \
  --cpus-per-task=20 \
  --pty bash -i

#sif=/work/dcibioinformatics-gatk-nf-mic-2024/sifs/dcibioinformaticsHTS-v0.8.sif
sif=/opt/apps/containers/community/mic-2024/2024-mic-singularity-image_latest.sif
cap=/hpc/group/mic-2024/references
wd=/work/mrl17
dictwd=/work/dcibioinformatics-gatk-nf-mic-2024

singularity shell \
--app picardtools \
--containall \
--no-home \
--bind .:/wd \
--bind ${cap}:${cap}:ro \
--bind ${dictwd}:${dictwd}:ro \
--bind ${wd}:${wd}:rw \
${sif}

dict=/work/dcibioinformatics-gatk-nf-mic-2024/annotation/GATK/hg38/v0/Homo_sapiens_assembly38.dict    
wd=/work/mrl17/annotation/Captures
cap=/hpc/group/mic-2024/references/exome_capture/agilent_all_exon_v6_S07604514_hs_hg38
baitbed=S07604514_Regions.bed
targbed=S07604514_Padded.bed
baitint=S07604514_Regions.interval_list
targint=S07604514_Padded.interval_list

# note:
# there are three available target/bait file sets available at
# /hpc/group/mic-2024/references/exome_capture
# I chose the first set for testing purposes

# --- convert bait file
java \
	-jar $PICARD_JAR BedToIntervalList \
	--INPUT ${cap}/${baitbed} \
	--OUTPUT ${wd}/${baitint} \
	--SEQUENCE_DICTIONARY ${dict} \
	--SORT true

# --- convert target file
java \
	-jar $PICARD_JAR BedToIntervalList \
	--INPUT ${cap}/${targbed} \
	--OUTPUT ${wd}/${targint} \
	--SEQUENCE_DICTIONARY ${dict} \
	--SORT true
