// ----- test stage1 input channel

/*
params.samplesheet_stg1 = "/mnt/data1/workspace/Methods/GATK_nf/Proc/stage1_samplesheet.csv"

// things to change in sample sheet script
// - remove periods from column names
// - remove row index column that has empty column header

Channel
    .fromPath( params.samplesheet_stg1, checkIfExists: true )
    .splitCsv( header: true )
    .map { row -> [[uid: row.stage1UID, 
                   rgid: row.RGID,
                   rgpu: row.RGPU,
                   rgsm: row.RGSM,
                   rglb: row.RGLB],
                   [file(row.parentdir + "/" + row.R1), 
                   file(row.parentdir + "/" + row.R2)]] }
//    .groupTuple( by: 0 )
//    .view { uid, rgid -> "$uid : $rgid" }
    .view()
*/

// ----- test stage2 input channel

params.samplesheet_stg2 = "/mnt/data1/workspace/Methods/GATK_nf/Proc/stage2_samplesheet.csv"

Channel
    .fromPath( params.samplesheet_stg2, checkIfExists: true )
    .splitCsv( header:true )
    .map { row -> [row.uid, 
                   row.seqidx_lane,  
		   file(row.parentdir + "/" + row.samfile)] }
    .groupTuple( by: 0 )
    .view()

// /opt/NGS/nextflow/22.10.3/nextflow run
