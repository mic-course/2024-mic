
workflow {

    Channel
        .fromPath( params.samplesheet_stg2, checkIfExists: true )
        .splitCsv( header:true )
        .map { row -> [row.uid,
                       //row.seqidx_lane,
                       file(row.parentdir + "/" + row.samfile)] }
        .groupTuple( by: 0 )
        .set { sort_bam_ch }

    Channel 
        .fromPath( params.gatk_genome_dir, checkIfExists: true )
        .first()
        .set { genome_ch }

    Channel 
        .fromPath( params.captures_dir, checkIfExists: true )
        .first()
        .set { captures_ch }

    Channel 
        .fromPath( params.tmpdir, checkIfExists: true )
	      .first()
	      .set{ tmp_ch }

    markDuplicates( sort_bam_ch, tmp_ch )
    collectHsMetrics( markDuplicates.out.markedbam,
                      markDuplicates.out.markedbai,
                      tmp_ch, 
                      genome_ch,
                      captures_ch )

}

process markDuplicates {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg2}/MarkDuplicates/${uid}", mode: "copy"
  tag "MarkDuplicates on $uid"
  label 'highresource'

  input:
  // tuple val(uid), val(seqlane), path(sortbam, stageAs: "lane_??/*") 
  tuple val(uid), path(sortbam)
  path tmp 

  output:
  tuple val(uid), path("*.bam"), emit: markedbam
  tuple val(uid), path("*.bai"), emit: markedbai 
  tuple val(uid), path("*")

  script:
  // def sortbamlist = sortbam.collect{"--INPUT $it"}.join(' ')

  """
  java ${params.java_options} -jar /scif/apps/picardtools/picard.jar MarkDuplicates \
  --INPUT ${sortbam} \
  --METRICS_FILE metrics.txt \
  --OUTPUT marked.sorted.bam \
  --CREATE_MD5_FILE true \
  --CREATE_INDEX true \
  --VALIDATION_STRINGENCY ${params.val_stringency} \
  --TMP_DIR ${tmp}
  """

}


process collectHsMetrics {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg2}/CollectHsMetrics/${uid}", mode: "copy"  
  tag "CollectHsMetrics on $uid"
  label 'highresource'
  
  input:
  tuple val(uid), path(markedbam)
  tuple val(uid), path(markedbai)
  path tmp 
  path ref 
  path captures 

  output:
  tuple val(uid), path("*")

  script:
  """
  java ${params.java_options} -jar /scif/apps/picardtools/picard.jar CollectHsMetrics \
  --INPUT ${markedbam} \
  --OUTPUT marked_hs_metrics.txt \
  --REFERENCE_SEQUENCE ${ref}/${params.gatk_genome} \
  --BAIT_INTERVALS ${captures}/${params.baits} \
  --TARGET_INTERVALS ${captures}/${params.targets} \
  --VALIDATION_STRINGENCY ${params.val_stringency} \
  --TMP_DIR ${tmp} \
  --CREATE_INDEX true \
  --CREATE_MD5_FILE true 
  """

}
