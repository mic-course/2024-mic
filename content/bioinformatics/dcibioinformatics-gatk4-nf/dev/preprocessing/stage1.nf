
workflow {

    Channel 
        .fromPath( params.samplesheet_stg1, checkIfExists: true )
        .splitCsv( header: true )
        .map { row -> [[uid: row.stage1UID, 
                      rgid: row.RGID,
                      rgpu: row.RGPU,
                      rgsm: row.RGSM,
                      rglb: row.RGLB],
                      [file(row.parentdir + "/" + row.R1), 
                      file(row.parentdir + "/" + row.R2)]] }
        .set { reads_ch }

    Channel 
        .fromPath( params.gatk_genome_dir, checkIfExists: true )
        .first()
        .set { genome_ch }

    Channel 
        .fromPath( params.tmpdir, checkIfExists: true )
	      .first()
	      .set{ tmp_ch }

    bwa( reads_ch, genome_ch )
    addOrReplaceReadGroups( bwa.out.outsam, tmp_ch )

}


process bwa {

  containerOptions '--app bwa'
  publishDir "${params.outdir}/${params.stg1}/bwa/${meta.uid}", mode: "copy"
  tag "bwa on ${meta.uid}"
  clusterOptions = '\
      --mem-per-cpu 15G \
      --account chsi \
      --partition chsi \
      --cpus-per-task 4'

  input:
  tuple val(meta), path(reads)
  path ref

  output:
  tuple val(meta), path("output.sam"), emit: outsam

  script:
  """
  bwa mem \
  -aM \
  -t ${params.threads} \
  ${ref}/${params.gatk_genome} \
  ${reads[0]} ${reads[1]} > output.sam
  """

}


process addOrReplaceReadGroups {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg1}/AddOrReplaceReadGroups/${meta.uid}", mode: "copy"
  tag "AddOrReplaceReadGroups on ${meta.uid}"
  label 'highresource'
  

  input:
  tuple val(meta), path(outsam)
  path tmp

  output:
  tuple val(meta), path("sorted.*")

  script:
  """
  java ${params.java_options} -jar /scif/apps/picardtools/picard.jar AddOrReplaceReadGroups \
  --INPUT ${outsam} \
  --OUTPUT sorted.bam \
  --RGID ${meta.rgid} \
  --RGPL ${params.seqplatform} \
  --RGSM ${meta.rgsm} \
  --RGPU ${meta.rgpu} \
  --RGLB ${meta.rglb} \
  --VALIDATION_STRINGENCY ${params.val_stringency} \
  --CREATE_INDEX true \
  --SORT_ORDER ${params.sort_order} \
  --TMP_DIR ${tmp}
  """

}
