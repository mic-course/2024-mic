srun \
  --partition=chsi-high \
  --account=mic-2024 \
  --nodes=1 \
  --ntasks-per-node=1 \
  --cpus-per-task=20 \
  --pty bash -i

#sif=/work/dcibioinformatics-gatk-nf-mic-2024/sifs/dcibioinformaticsR-v2.2.sif
sif=/opt/apps/containers/community/mic-2024/2024-mic-singularity-image_latest.sif
wd=/work/mic-2024-gatk
raw=/hpc/group/mic-2024

singularity exec \
  --containall \
  --no-home \
  --bind ${wd}:${wd}:rw \
  --bind ${raw}:${raw}:rw \
  ${sif} \
  R

library(tidyverse)
#library(Biostrings)

# paths
wd <- "/work/mic-2024-gatk"
raw <- "/hpc/group/mic-2024/rawdata/fastq_dir"
manifest.path <- "/hpc/group/mic-2024/rawdata/sra_metadata.csv"
proc <- "/hpc/group/mic-2024/proc"
#subfiles <- file.path(wd, "downsample")

if(!dir.exists(file.path(proc, "stage1"))){
  dir.create(file.path(proc,"stage1"))
}

# include manifest!
m <- read.csv(manifest.path, header = TRUE) %>%
  mutate(Model = gsub(" ", "", Model))

# create a dataframe of sequences files
data.frame(Filename = list.files(raw, 
                                 pattern = "fastq.gz"),
           full.path = list.files(raw, 
                                  pattern = "fastq.gz",
                                  recursive = TRUE, 
                                  full.names = TRUE)) %>%
  mutate(Filename2 = gsub(".fastq.gz", "", Filename)) %>%
  separate(Filename2, 
           into = c("libName", 
                    "read"), 
           sep = "_", 
           remove = TRUE) -> seq.df

nrow(seq.df)/2 # 112 samples
length(unique(seq.df$libName)) #112 samples

# get the header from each fastq -->
# not really necessary for this dataset since there isn't
# any metadata stored in the FASTQ header...
# FQs <- seq.df$full.path
# header.list <- list()
# 
# for(i in 1:length(FQs)){
#   tmp <- Biostrings::readDNAStringSet(FQs[i], nrec = 1, format = "fastq")
#   header.list[[i]] <- names(tmp)
# }
# seq.df$header <- unlist(header.list)
# seq.df %>%
#   separate(header, 
#            into = c(NA, NA, "flowcell.id", "flowcell.lane"), 
#            sep = ":", 
#            remove = F, 
#            extra = "drop") %>%
#   separate(header,
#            into = c(NA, "part2"),
#            sep = " ",
#            remove = FALSE) %>% 
#   separate(part2,
#            into = c(NA, NA, NA, "sample.barcode"),
#            sep = ":",
#            remove = TRUE) -> seq.df.out

# seq.df <- seq.df.out

# make gatk_samplesheet.csv ----------------------------------------------------
# identify r1 and r2 pairs
seq.df %>%
  left_join(m, by = c("libName" = "Run")) %>%
  mutate(parentdir = dirname(full.path),
         read = gsub("^", "R", read)) %>%
  group_by(libName) %>%  
  mutate(RGID = paste0(Model, ".", libName),
         RGPU = paste0(Model, ".", libName),
         RGSM = libName,
         RGLB = libName) %>% 
  select(stage1UID = libName,
         RGID,
         RGSM,
         RGPU,
         RGLB,
         read,
         parentdir,
         Filename) %>%
  pivot_wider(names_from = read,
              values_from = Filename) -> seq.pairs.df
dim(seq.pairs.df)
head(seq.pairs.df)

# --- just take one sample for initial test
#dir(subfiles)
#sub.df <- seq.pairs.df %>% 
#  filter(stage1UID %in% c("SRR12415478"))
# need to modfiy the path to point to the downsampled fastqs
# sub.df$parentdir <- subfiles
# sub.df$R1 <- "SRR12415478_1_sub.fastq.gz"
# sub.df$R2 <- "SRR12415478_2_sub.fastq.gz"


currf <- file.path(proc, "stage1", "stage1_samplesheet.csv")
write.csv(seq.pairs.df, 
          row.names = FALSE, 
          quote = FALSE,
          currf)
