# downsample fastqs using seqtk within a loop

cd /hpc/group/mic-2024/rawdata/fastq_dir
# --- all below are Illumina NovaSeq 6000
# SRR12415578 -> normal (MED064)
# SRR12415554 -> tumor (MED085)
# SRR12415556 -> normal (MED085)
# SRR12415478 -> tumor (MED091)

files1=$(ls SRR12415578*.fastq.gz) 
files2=$(ls SRR12415554*.fastq.gz)
files3=$(ls SRR12415556*.fastq.gz)
files4=$(ls SRR12415478*.fastq.gz)

seqtk=/scif/apps/seqtk/seqtk-1.3/seqtk # path to seqtk (actual path in HTS container)
out=/hpc/group/mic-2024/subset_data/fastq_dir # path to where you want your downsampled fastqs to go
sub=0.01 # fraction to downsample to
seed=100 # set seed
N=2 # number of cores

for file in ${files1}; 
do 
  ((i=i%N)); ((i++==0)) && wait
  n="echo $(cat ${file} | wc -l)/4 | bc" # divide by four because there are four lines of information per read
  name=${file%.fastq.gz} # get filename w/o extension
  ${seqtk} sample -s${seed} ${file} ${sub}*${n} > ${out}/${name}_sub.fastq.gz & 
done 
