
workflow {

    Channel 
        .fromPath( params.samplesheet_stg3, checkIfExists: true )
        .splitCsv( header: true )
        .map { row -> [row.stage3uid, 
                       file(row.parentdir + "/" + row.bamfilename), 
                       file(row.parentdir + "/" + row.baifilename)] }
        .set { marked_bam_ch }

    Channel 
        .fromPath( params.gatk_genome_dir, checkIfExists: true )
        .first()
        .set { genome_ch }

    Channel 
        .fromPath( params.captures_dir, checkIfExists: true )
        .first()
        .set { captures_ch }

    Channel 
        .fromPath( params.tmpdir, checkIfExists: true )
	      .first()
	      .set { tmp_ch }

    baseRecalibrator( marked_bam_ch,
                      genome_ch,
                      captures_ch,
                      tmp_ch )
    
    bqsrfiles = marked_bam_ch.join(baseRecalibrator.out.recaltab, by: 0)
    /*applyBQSR( marked_bam_ch,
               baseRecalibrator.out.recaltab,
               genome_ch,
               tmp_ch )*/
    applyBQSR( bqsrfiles,
               genome_ch,
               tmp_ch )    
    collectHsMetrics( applyBQSR.out.recalbam, 
                      tmp_ch,
                      genome_ch,
                      captures_ch )

}

process baseRecalibrator {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg3}/BaseRecalibrator/${uid}", mode: "copy"
  tag "BaseRecalibrator on $uid"
  label 'highresource'
  
  input:
  tuple val(uid), path(markedbam), path(markedbai) 
  path ref
  path captures 
  path tmp 

  output:
  tuple val(uid), path("recal.table"), emit: recaltab
  tuple val(uid), path("*")

  script:
  """
  gatk --java-options ${params.java_options} BaseRecalibrator \
  --input ${markedbam} \
  --known-sites ${ref}/${params.dbsnp} \
  --known-sites ${ref}/${params.indel1} \
  --known-sites ${ref}/${params.indel2} \
  --output recal.table \
  --reference ${ref}/${params.gatk_genome} \
  --intervals ${captures}/${params.targets} \
  --tmp-dir ${tmp}
  """

}


process applyBQSR {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg3}/ApplyBQSR/${uid}", mode: "copy"
  tag "ApplyBQSR on $uid"
  label 'highresource'

  input:
  tuple val(uid), path(markedbam), path(markedbai), path(recaltab)
  path ref 
  path tmp 

  output:
  tuple val(uid), path("recal.bam"), emit: recalbam 
  tuple val(uid), path("*")

  script:
  //def bqsrfiles = marked_bam_ch.join(recaltab, by: 0)

  """
  gatk --java-options ${params.java_options} ApplyBQSR \
  --input ${markedbam} \
  --output recal.bam \
  --create-output-bam-md5 true \
  --reference ${ref}/${params.gatk_genome} \
  --bqsr-recal-file ${recaltab} \
  --create-output-bam-index true \
  --tmp-dir ${tmp}
  """

}


process collectHsMetrics {

  publishDir "${params.outdir}/${params.stg3}/CollectHsMetrics/${uid}", mode: "copy"  
  tag "CollectHsMetrics on $uid"
  label 'highresource'

  input:
  tuple val(uid), path(recalbam)
  path tmp 
  path ref 
  path captures 

  output:
  tuple val(uid), path("*")

  script:
  """
  java ${params.java_options} -jar /scif/apps/picardtools/picard.jar CollectHsMetrics \
  --INPUT ${recalbam} \
  --OUTPUT marked_hs_metrics.txt \
  --REFERENCE_SEQUENCE ${ref}/${params.gatk_genome} \
  --BAIT_INTERVALS ${captures}/${params.baits} \
  --TARGET_INTERVALS ${captures}/${params.targets} \
  --VALIDATION_STRINGENCY ${params.val_stringency} \
  --TMP_DIR ${tmp} \
  --CREATE_INDEX true \
  --CREATE_MD5_FILE true 
  """

}
