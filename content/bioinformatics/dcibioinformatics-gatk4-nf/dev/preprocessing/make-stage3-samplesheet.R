srun \
--partition=chsi-high \
--account=mic-2024 \
--nodes=1 \
--ntasks-per-node=1 \
--cpus-per-task=20 \
--pty bash -i

#sif=/work/dcibioinformatics-gatk-nf-mic-2024/sifs/dcibioinformaticsR-v2.2.sif
sif=/opt/apps/containers/community/mic-2024/2024-mic-singularity-image_latest.sif
wd=/work/mic-2024-gatk

singularity exec \
--containall \
--no-home \
--bind ${wd}:${wd}:rw \
${sif} \
R

library(tidyverse)

# paths      
wd <- "/work/mic-2024-gatk"
proc <- file.path(wd, "proc")
out <- file.path(proc, "stage3/stage3_samplesheet.csv")

if(!dir.exists(file.path(proc, "stage3"))){
  dir.create(file.path(proc,"stage3"))
}

# make stage3 csv  
bams <- list.files(file.path(proc, "stage2"), 
                   pattern = "marked.sorted.bam$", 
                   recursive = TRUE, 
                   full.names = TRUE) # pull marked.sorted.bams generated by MarkDuplicates
bais <- list.files(file.path(proc, "stage2"), 
                   pattern = "marked.sorted.bai$", 
                   recursive = TRUE, 
                   full.names = TRUE) # pull bam indexes for marked.sorted.bam files

data.frame(bams,
           bais) %>% 
  separate(bams, 
           into = c(NA, NA, NA,
                    NA, NA, NA,
                    "stage3uid", NA),
           sep = "/",
           remove = FALSE) %>% 
  mutate(parentdir = dirname(bams),
         bamfilename = basename(bams),
         baifilename = basename(bais)) %>% 
  relocate(stage3uid) -> stage3.df

dim(stage3.df) # 112   6

# write stage3.samples to out
write.csv(stage3.df, 
          row.names = FALSE, 
          quote = FALSE,
          out)
