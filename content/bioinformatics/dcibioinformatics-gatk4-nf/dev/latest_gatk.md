# Compare our pipeline with the latest best practice
022024

The latest version of GATK is:  4.5.0.0
Tools for the latest GATK: https://gatk.broadinstitute.org/hc/en-us/articles/21904996835867--Tool-Documentation-Index
All functions and steps were confirmed based on the latest tool documentation. 

The version we used in the legacy and nf pipelines: 4.4.0.0. 

## Preprocessing 
This version of the best practice was updated on Dec 11th, 2023. 
https://gatk.broadinstitute.org/hc/en-us/articles/360035535912-Data-pre-processing-for-variant-discovery

Checked the suggested steps and each step's description in the latest GATK version's toolkit documentation. 

**The new pipeline suggested starting the analysis from the uBAM [unmapped BAM (uBAM) format](https://gatk.broadinstitute.org/hc/en-us/articles/360035532132-uBAM-Unmapped-BAM-Format). But BWA does not take the bam files. So, we will not incorporate this fundamental change at this time. All the other discrepancies are related to the uBAM change. So, we will keep the preprocessing steps unchanged.**

* Map to Reference
	+ Changing: https://gatk.broadinstitute.org/hc/en-us/articles/4403687183515--How-to-Generate-an-unmapped-BAM-from-FASTQ-or-aligned-BAM. The unmapped bams are generated with the read group added before the BWA. (**Not incorporated**)
	+ `BWA` -- should take the bam files now. 
	+ `MergeBamAlignments` -- I do not recall if we ever used this tool before. (**Not Added**)
		- The unmapped bam may contain useful information that will be lost in the conversion to fastq (meta-data like sample alias, library, barcodes, etc., and read-level tags.) This tool takes an unaligned bam with meta-data, and the aligned bam produced by calling {@link SamToFastq} and then passing the result to an aligner/mapper.
* Read group
	+ This is gone. Performed before the BWA. (**Not incorporated**)
	+ The input is expected to include the read groups. So the `AddOrReplaceReadGroups` step was not included here. 
	+ Also, we sorted the order in this step. Not sure if this can save us from the extra `SortSam` step below. 
* Mark Duplicates
	+ `MarkDuplicates` -- unchanged.
	+ `SortSam` -- suggested to be performed after the `MarkDuplicates`; we did not include this. (**Not Added**)
	+ Alternatively, use `MarkDuplicatesSpark`, which performs both the duplicate marking step and the sort step. (**Not incorporated**)
* Base (Quality Score) Recalibration
	+ `BaseRecalibrator` -- unchanged.
	+ `ApplyBQSR` -- unchanged.
* Metrics collection
	+ `collectHsMetrics` -- unchanged. We also included this step before and after the base recalibration. 

## Somatic
The PON documentation was updated on Jan 9th, 2024: 
https://gatk.broadinstitute.org/hc/en-us/articles/21905137669019-CreateSomaticPanelOfNormals-BETA

This version of the somatic best practice was updated on March 20th, 2023:
https://gatk.broadinstitute.org/hc/en-us/articles/360035894731-Somatic-short-variant-discovery-SNVs-Indels

Somatic pipeline with the PON: 
https://gatk.broadinstitute.org/hc/en-us/articles/360035531132--How-to-Call-somatic-mutations-using-GATK4-Mutect2

Checked the suggested steps and each step's description in the latest GATK version's toolkit documentation. 

* PON 
	+ `Mutect2` for normal samples -- unchanged. (https://gatk.broadinstitute.org/hc/en-us/articles/21905083931035-Mutect2)
	+ `GenomicsDBImport` -- unchanged.
	+ `CreateSomaticPanelOfNormals` -- unchanged.
	+ `GatherVcfsCloud` -- unchanged. 
	+ `sort` and `index` with `bcftools` -- unchanged. 
* Somatic call 
	+ `Mutect2` -- unchanged. (https://gatk.broadinstitute.org/hc/en-us/articles/21905083931035-Mutect2) (Previously, we observed a version of documentation suggesting applying Mutect2 for each chromosome instead of against the interval list. But, that could be an older version. This step can remain unchanged for now.)
	+ `LearnReadOrientationModel` -- We also included this step; unchanged. 
	+ `GetPileupSummaries` for tumor samples -- unchanged. 
	+ `GetPileupSummaries` for normal samples -- unchanged.
	+ `CalculateContamination` -- the recommended usage example remains unchanged. However, the next step needs the segment.tsv table. So we will need to raise the `--tumor-segmentation segments.tsv`flag. *This flag was previous deprecated, but now active again.* (**Added**)
	+ `FilterMutectCalls` -- `--tumor-segmentation segments.tsv` needed. (**Added**)


## Germline 
This version of the best practice was updated on Feb 02, 2024. 
https://gatk.broadinstitute.org/hc/en-us/articles/360035535932-Germline-short-variant-discovery-SNPs-Indels
Checked the suggested steps and each step's description in the latest GATK version's toolkit documentation. 

* Call variants per-sample
	+ `HaplotypeCaller` -- unchanged. 
* Consolidate GVCFs
	+ `GenomicsDBImport` -- unchanged. 
* Joint-Call Cohort
	+ `GenotypeGVCFs` -- unchanged. 
	+ `GatherVcfsCloud` -- unchanged. We split the call into multiple intervals, so we need to merge it. 
	+ `sort` and `index` with `bcftools` -- unchanged.
* Filter variants before VQSR
	+ `VariantFiltration` -- unchanged. Addtional step added by us. 
* Filter variants by Variant (Quality Score) Recalibration
	+ `VariantRecalibrator` -- unchanged.
	+ `ApplyVQSR` -- unchanged. Note that the ApplyVQSR tool page suggested using the genome fasta as the reference, but we did not include this in our legacy pipeline. Also, the example `--truth-sensitivity-filter-level` flag suggested 99.0, but we used the number 99.7 in the legacy pipeline. 