
def helpMessage() {
  log.info"""
  Usage:
  The typical command for running the pipeline is as follows:
    nextflow run -c gatk_germline_proc.config stage4.nf -with-report gatk_stage4.html
  """.stripIndent()
}

workflow {

  if (params.help) {
      helpMessage()
      exit 0
  }

  Channel 
      .fromPath( params.samplesheet_stg4, checkIfExists: true )
      .splitCsv( header: true )
      .map { row -> [row.stage4uid, 
                     file(row.bams), 
                     file(row.bais)] }
      .set { recal_bam_ch }
  
  Channel 
      .fromPath( params.gatk_genome_dir, checkIfExists: true )
      .first()
      .set { genome_ch }

  Channel 
      .fromPath( params.tmpdir, checkIfExists: true )
      .first()
      .set { tmp_ch }

  Channel
      .fromPath( params.captures_dir, checkIfExists: true )
      .first()
      .set { captures_ch }

  HaplotypeCaller( recal_bam_ch,
                   tmp_ch,
                   genome_ch,
                   captures_ch )

}


process HaplotypeCaller {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg4}/HaplotypeCaller/${uid}", mode: "copy"
  tag "HaplotypeCaller on ${uid}"
  label 'highresource'

  input:
  tuple val(uid), path(recalbam), path(recalbai)
  path tmp
  path ref
  path captures

  output:
  path "*"

  script:
  """
  gatk --java-options ${params.java_options} HaplotypeCaller \
    --input ${recalbam} \
    --output output.g.vcf.gz \
    -ERC GVCF \
    --reference ${ref}/${params.gatk_genome} \
    --dbsnp ${ref}/${params.dbsnp} \
    --intervals ${captures}/${params.targets} \
    --tmp-dir ${tmp}
  """

}
