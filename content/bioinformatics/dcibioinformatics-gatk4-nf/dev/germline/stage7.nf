
def helpMessage() {
  log.info"""
  Usage:
  The typical command for running the pipeline is as follows:
    nextflow run -c gatk_germline_proc.config stage7.nf -with-report gatk_stage7.html
  """.stripIndent()
}

workflow {

    if (params.help) {
        helpMessage()
        exit 0
    }

    Channel 
        .fromPath( params.samplesheet_stg7, checkIfExists: true )
        .splitCsv( header: true )
        .map { row -> [row.stage7uid, 
                       file(row.parentdir + "/" + row.vcffilename), 
                       file(row.parentdir + "/" + row.tbifilename)] }
        .set { vcf_ch }

    Channel 
        .fromPath( params.tmpdir, checkIfExists: true )
        .first()
        .set { tmp_ch }

    VariantFiltration( vcf_ch,
                       tmp_ch  )
}


process VariantFiltration {

  containerOptions '--app gatk'
  publishDir "${params.outdir}/${params.stg7}/VariantFiltration/${uid}", mode: "copy"
  tag "Hard Variant Filtration on ${uid}"
  label 'lowresource'

  input:
  tuple val(uid), path(vcffile), path(tbifile)
  path tmp 

  output:
  tuple val(uid), path("filt.vcf.gz"), emit: filtvcf
  tuple val(uid), path("*")
  path ".command.log"

  script:
  """
  gatk --java-options ${params.java_options} VariantFiltration \
  --variant ${vcffile} \
  --output filt.vcf.gz \
  --filter-expression "ExcessHet > 54.69" \
  --filter-name "ExcessHet" \
  -OVI  \
  --tmp-dir ${tmp}
  """

}
