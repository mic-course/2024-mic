def helpMessage() {
  log.info"""
  Usage:
  The typical command for running the pipeline is as follows:
    nextflow run -c gatk_germline_proc.config stage5.nf -with-report gatk_stage5.html
  """.stripIndent()
}


workflow {

  if (params.help) {
      helpMessage()
      exit 0
  }

  Channel 
      .fromPath( params.subintervals_stg5, checkIfExists: true )
      .splitCsv( header: true )
      .map { row -> row.subIDs }
      .set { subintid_ch }

  Channel 
      .fromPath( params.subintervals_stg5, checkIfExists: true )
      .splitCsv( header: true )
      .map { row -> file(row.subs) }
      .set { subint_ch }

  Channel 
      .fromPath( params.samplemap_stg5, checkIfExists: true )
      .first()
      .set { map_ch }

  Channel 
      .fromPath( params.gatk_genome_dir, checkIfExists: true )
      .first()
      .set { genome_ch }

  Channel 
      .fromPath( params.tmpdir, checkIfExists: true )
      .first()
      .set { tmp_ch }

  subintid_ch
      .merge(subint_ch)
      .set { subs_ch }

  GenomicsDB( subs_ch,
              map_ch,
              genome_ch,
              tmp_ch )
  GenotypeGVCFs( GenomicsDB.out.subdb,
                 genome_ch,
                 tmp_ch )

}


process GenomicsDB {

  containerOptions "--app gatk"
  publishDir "${params.outdir}/${params.stg5}/GenomicsDB", mode: "copy"
  tag "GenomicsDBImport"
  label 'highresource'

  input:
  each sub
  path map
  path ref
  path tmp 

  output:
  path("subint*", type: "dir", emit: "subdb")
  path ".command.log"

  script:
  """
  gatk --java-options "-Xmx16G -Xms8G" GenomicsDBImport \
  --reference ${ref}/${params.gatk_genome} \
  --intervals ${sub[1]} \
  --genomicsdb-workspace-path ${sub[0]} \
  --overwrite-existing-genomicsdb-workspace true \
  --genomicsdb-shared-posixfs-optimizations true \
  --merge-input-intervals true \
  --bypass-feature-reader true \
  --sample-name-map ${map} \
  --tmp-dir ${tmp}
  """

}

process GenotypeGVCFs {

  containerOptions "--app gatk"
  publishDir "${params.outdir}/${params.stg5}/GenotypeGVCFs/${subdb}", mode: "copy"
  tag "GenotypeGVCFs"
  label 'highresource'

  input:
  path subdb
  path ref 
  path tmp 

  output:
  path "output.vcf.*"
  path ".command.log"

  script:
  """
  gatk --java-options ${params.java_options} GenotypeGVCFs \
  --reference ${ref}/${params.gatk_genome} \
  --variant gendb://${subdb} \
  --genomicsdb-shared-posixfs-optimizations true \
  --output output.vcf.gz \
  --tmp-dir ${tmp}
  """

}
