def helpMessage() {
  log.info"""
  Usage:
  The typical command for running the pipeline is as follows:
    nextflow run -c gatk_germline_proc.config stage6.nf -with-report gatk_stage6.html
  """.stripIndent()
}

workflow {

  if (params.help) {
      helpMessage()
      exit 0
  }

  Channel 
      .fromPath( params.samplesheet_stg6, checkIfExists: true )
      .splitCsv( header: true )
      .map { row -> [row.intervallist,
                     file(row.gvcfpath)] }
      .groupTuple( by: 0 )
      .set { subint_ch }

  Channel 
      .fromPath( params.tmpdir, checkIfExists: true )
      .first()
      .set { tmp_ch }

  mergevariant( subint_ch,
                tmp_ch )
  sort( mergevariant.out.mergedvar,
        tmp_ch )

}


process mergevariant {

  containerOptions "--app gatk"
  publishDir "${params.outdir}/${params.stg6}/mergevariant", mode: "copy"
  tag "Merging sub intervals"
  label 'lowresource'

  input:
  tuple val(kit), path(subints, stageAs: "subint??/*")
  path tmp

  output:
  path "*"
  path "merged_variants.vcf.gz", emit: mergedvar
  path ".command.log"

  script:
  def vcflist = subints.collect{"--input $it"}.join(' ')

  """
  gatk --java-options ${params.java_options} GatherVcfsCloud \
  ${vcflist} \
  --output merged_variants.vcf.gz \
  --create-output-variant-index true \
  --tmp-dir ${tmp}
  """

}


process sort {

  containerOptions "--app bcftools"
  publishDir "${params.outdir}/${params.stg6}/Sort", mode: "copy"
  tag "Sorting merged variant"
  label 'lowresource'

  input:
  path mergedvar
  path tmp 

  output:
  path "*"
  path ".command.log"

  script:
  """
  bcftools sort \
    --output merged_sort_variants.vcf.gz \
    --output-type z \
    --temp-dir ${tmp} \
    ${mergedvar}

  bcftools index \
    --tbi \
    --output merged_sort_variants.vcf.gz.tbi \
    merged_sort_variants.vcf.gz

  """

}

