
def helpMessage() {
  log.info"""
  Usage:
  The typical command for running the pipeline is as follows:
    nextflow run -c gatk_germline_proc.config stage9.nf -with-report gatk_stage9.html
  """.stripIndent()
}

workflow {

  if (params.help) {
      helpMessage()
      exit 0
  }

  Channel 
      .fromPath( params.samplesheet_stg9, checkIfExists: true )
      .splitCsv( header: true )
      .map { row -> [row.stage9uid, 
                     file(row.vcffinal), 
                     file(row.tbifinal), 
                     file(row.vcffilt), 
                     file(row.tbifilt), 
                     file(row.vcfraw), 
                     file(row.tbiraw)] }
      .set { variant_ch }
  
  Channel 
      .fromPath( params.gatk_genome_dir, checkIfExists: true )
      .first()
      .set { genome_ch }

  Channel 
      .fromPath( params.tmpdir, checkIfExists: true )
      .first()
      .set { tmp_ch }

  Channel
      .fromPath( params.captures_dir, checkIfExists: true )
      .first()
      .set { captures_ch }

  VarientMetricsFinal( variant_ch,
                       tmp_ch,
                       genome_ch,
                       captures_ch )
  VarientMetricsFilt( variant_ch,
                      tmp_ch,
                      genome_ch,
                      captures_ch )
  VarientMetricsRaw( variant_ch,
                     tmp_ch,
                     genome_ch,
                     captures_ch )
}


process VarientMetricsFinal {

  publishDir "${params.outdir}/${params.stg9}/VarientMetrics/${uid}", mode: "copy"
  tag "Collects per-sample and aggregate (spanning all samples) metrics from the provided VCF file."
  label 'highresource'

  input:
  tuple val(uid), path(vcffinal), path(tbifinal), path(vcffilt), path(tbifilt), path(vcfraw), path(tbiraw)
  path tmp
  path ref
  path captures
  

  output:
  path "*"
  path ".command.log"

  script:
  """
  java ${params.java_options} -jar /scif/apps/picardtools/picard.jar CollectVariantCallingMetrics \
  --INPUT ${vcffinal} \
  --DBSNP ${ref}/${params.dbsnp}\
  --OUTPUT final \
  --THREAD_COUNT 10 \
  --TARGET_INTERVALS ${captures}/${params.targets} \
  --TMP_DIR ${tmp}
  """
}

process VarientMetricsFilt {

  publishDir "${params.outdir}/${params.stg9}/VarientMetrics/${uid}", mode: "copy"
  tag "Collects per-sample and aggregate (spanning all samples) metrics from the provided VCF file."
  label 'highresource'

  input:
  tuple val(uid), path(vcffinal), path(tbifinal), path(vcffilt), path(tbifilt), path(vcfraw), path(tbiraw)
  path tmp
  path ref
  path captures
  

  output:
  path "*"
  path ".command.log"

  script:
  """
  java ${params.java_options} -jar /scif/apps/picardtools/picard.jar CollectVariantCallingMetrics \
  --INPUT ${vcffilt} \
  --DBSNP ${ref}/${params.dbsnp}\
  --OUTPUT filt \
  --THREAD_COUNT 10 \
  --TARGET_INTERVALS ${captures}/${params.targets} \
  --TMP_DIR ${tmp}
  """
}

process VarientMetricsRaw {

  publishDir "${params.outdir}/${params.stg9}/VarientMetrics/${uid}", mode: "copy"
  tag "Collects per-sample and aggregate (spanning all samples) metrics from the provided VCF file."
  label 'highresource'

  input:
  tuple val(uid), path(vcffinal), path(tbifinal), path(vcffilt), path(tbifilt), path(vcfraw), path(tbiraw)
  path tmp
  path ref
  path captures
  

  output:
  path "*"
  path ".command.log"

  script:
  """
  java ${params.java_options} -jar /scif/apps/picardtools/picard.jar CollectVariantCallingMetrics \
  --INPUT ${vcfraw} \
  --DBSNP ${ref}/${params.dbsnp}\
  --OUTPUT raw \
  --THREAD_COUNT 10 \
  --TARGET_INTERVALS ${captures}/${params.targets} \
  --TMP_DIR ${tmp}
  """
}
