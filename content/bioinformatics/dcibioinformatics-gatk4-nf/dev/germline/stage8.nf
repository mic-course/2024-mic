def helpMessage() {
  log.info"""
  Usage:
  The typical command for running the pipeline is as follows:
    nextflow run -c gatk_germline_proc.config stage8.nf -with-report gatk_stage8.html
  """.stripIndent()
}

workflow {

  if (params.help) {
      helpMessage()
      exit 0
  }

  Channel 
      .fromPath( params.samplesheet_stg8, checkIfExists: true )
      .splitCsv( header: true )
      .map { row -> [row.stage8uid, 
                     file(row.parentdir + "/" + row.vcffilename), 
                     file(row.parentdir + "/" + row.tbifilename)] }
      .set { vcf_ch }

  Channel 
      .fromPath( params.tmpdir, checkIfExists: true )
      .first()
      .set { tmp_ch }
   
  Channel 
      .fromPath( params.gatk_genome_dir, checkIfExists: true )
      .first()
      .set { genome_ch }

  MakeSitesOnlyVcf( vcf_ch,
                    tmp_ch )

  /*
  The tbi and idx inputs are not specified in the scripts but are needed in the input. 
  Otherwise, the error will report that no index is found. 
  */
  VariantRecalibrator_Indels( MakeSitesOnlyVcf.out.sitevcfout,
                              genome_ch,
                              tmp_ch )
  VariantRecalibrator_SNP( MakeSitesOnlyVcf.out.sitevcfout,
                           genome_ch,
                           tmp_ch )

  /* 
  Join the output channels to make sure the files for the same uid are processed. 
  */
  vqsrindelfiles = vcf_ch.join(VariantRecalibrator_Indels.out.vrindelout, by: 0)
  VQSR_Indel( vqsrindelfiles,
              tmp_ch )

  vqsrsnpfiles = VQSR_Indel.out.vqsrout.join(VariantRecalibrator_SNP.out.vrsnpout, by: 0)
  VQSR_SNP( vqsrsnpfiles,
            tmp_ch )

}


process MakeSitesOnlyVcf {

  publishDir "${params.outdir}/${params.stg8}/MakeSitesOnlyVcf/${uid}", mode: "copy"
  tag "Site-level vcf to speed up VQSR"
  label 'highresource'

  input:
  tuple val(uid), path(vcffile), path(tbifile)
  path tmp

  output:
  path "*"
  tuple val(uid), path("sites.vcf.gz"), path("sites.vcf.gz.tbi"), emit: sitevcfout
  path ".command.log"

  script:
  """
  java ${params.java_options} -jar /scif/apps/picardtools/picard.jar MakeSitesOnlyVcf \
  --INPUT ${vcffile} \
  --OUTPUT sites.vcf.gz \
  --TMP_DIR ${tmp}
  """
}


process VariantRecalibrator_Indels {

  containerOptions "--app gatk"
  publishDir "${params.outdir}/${params.stg8}/VariantRecalibrator_Indels/${uid}", mode: "copy"
  tag "Indel recal step"
  label 'highresource'

  input:
  tuple val(uid), path(sitevcf), path(sitevcftbi)
  path ref
  path tmp 

  output:
  path "*"
  tuple val(uid), path("indels.recal.vcf"), path("indels.recal.vcf.idx"), path("indels.tranches"), emit: vrindelout
  path ".command.log"

  script:
  """
  gatk --java-options ${params.java_options} VariantRecalibrator \
    --variant ${sitevcf} \
    --trust-all-polymorphic \
    --truth-sensitivity-tranche 100.0 \
    --truth-sensitivity-tranche 99.95 \
    --truth-sensitivity-tranche 99.9 \
    --truth-sensitivity-tranche 99.5 \
    --truth-sensitivity-tranche 99.0 \
    --truth-sensitivity-tranche 97.0 \
    --truth-sensitivity-tranche 96.0 \
    --truth-sensitivity-tranche 95.0 \
    --truth-sensitivity-tranche 94.0 \
    --truth-sensitivity-tranche 93.5 \
    --truth-sensitivity-tranche 93.0 \
    --truth-sensitivity-tranche 92.0 \
    --truth-sensitivity-tranche 91.0 \
    --truth-sensitivity-tranche 90.0 \
    --use-annotation FS \
    --use-annotation ReadPosRankSum \
    --use-annotation MQRankSum \
    --use-annotation QD \
    --use-annotation SOR \
    --mode INDEL \
    --max-gaussians 4 \
    --resource:mills,known=false,training=true,truth=true,prior=12.0 ${ref}/${params.indel2} \
    --resource:dbsnp,known=true,training=false,truth=false,prior=2.0 ${ref}/${params.dbsnp} \
    --output indels.recal.vcf \
    --tranches-file indels.tranches \
    --tmp-dir ${tmp}

  """

}

process VariantRecalibrator_SNP {

  containerOptions "--app gatk"
  publishDir "${params.outdir}/${params.stg8}/VariantRecalibrator_SNP/${uid}", mode: "copy"
  tag "SNP recal step"
  label 'highresource'

  input:
  /* This does not work: tuple sitevcf */
  tuple val(uid), path(sitevcf), path(sitevcftbi)
  path ref
  path tmp 

  output:
  path("*")
  tuple val(uid), path("snps.recal.vcf"), path("snps.recal.vcf.idx"), path("snps.tranches"), emit: vrsnpout
  path ".command.log"

  script:
  """
  gatk --java-options ${params.java_options} VariantRecalibrator \
	--variant ${sitevcf} \
    --trust-all-polymorphic \
    --truth-sensitivity-tranche 100.0 \
    --truth-sensitivity-tranche 99.95 \
    --truth-sensitivity-tranche 99.9 \
    --truth-sensitivity-tranche 99.5 \
    --truth-sensitivity-tranche 99.0 \
    --truth-sensitivity-tranche 97.0 \
    --truth-sensitivity-tranche 96.0 \
    --truth-sensitivity-tranche 95.0 \
    --truth-sensitivity-tranche 94.0 \
    --truth-sensitivity-tranche 93.5 \
    --truth-sensitivity-tranche 93.0 \
    --truth-sensitivity-tranche 92.0 \
    --truth-sensitivity-tranche 91.0 \
    --truth-sensitivity-tranche 90.0 \
    --use-annotation QD \
    --use-annotation MQ \
    --use-annotation MQRankSum \
    --use-annotation ReadPosRankSum \
    --use-annotation FS \
    --use-annotation SOR  \
    --mode SNP \
    --max-gaussians 6 \
    --resource:hapmap,known=false,training=true,truth=true,prior=15.0 ${ref}/${params.hapmap} \
    --resource:omni,known=false,training=true,truth=true,prior=12.0 ${ref}/${params.omni} \
    --resource:1000G,known=false,training=true,truth=false,prior=10.0 ${ref}/${params.phase} \
    --resource:dbsnp,known=true,training=false,truth=false,prior=2.0 ${ref}/${params.dbsnp} \
    --output snps.recal.vcf \
    --tranches-file snps.tranches \
    --tmp-dir ${tmp}
  """

}

process VQSR_Indel {

  containerOptions "--app gatk"
  publishDir "${params.outdir}/${params.stg8}/VQSR_Indel/${uid}", mode: "copy"
  tag "Indel VQSR"
  label 'highresource'

  input:
  tuple val(uid), path(vcffile), path(tbifile), path(indelrecal), path(indelrecalidx), path(indeltranches)
  path tmp 

  output:
  path "*"
  tuple val(uid), path("indel_recal.vcf.gz"), path("indel_recal.vcf.gz.tbi"), emit: vqsrout 
  path ".command.log"

  script:
  """
  gatk --java-options ${params.java_options} ApplyVQSR \
    --variant ${vcffile} \
    --recal-file ${indelrecal} \
    --tranches-file ${indeltranches} \
    --truth-sensitivity-filter-level 99.7 \
    --mode INDEL \
    -OVI \
    --output indel_recal.vcf.gz \
    --tmp-dir ${tmp}
  """

}

process VQSR_SNP {

  containerOptions "--app gatk"
  publishDir "${params.outdir}/${params.stg8}/VQSR_SNP/${uid}", mode: "copy"
  tag "SNP VQSR"
  label 'highresource'

  input:
  tuple val(uid), path(vqsrindel), path(vqsrindeltbi), path(snprecal), path(snprecalidx), path(snptranches)
  path tmp 

  output:
  path "*"
  path ".command.log"

  script:
  """
  gatk --java-options ${params.java_options} ApplyVQSR \
    --variant ${vqsrindel} \
    --recal-file ${snprecal} \
    --tranches-file ${snptranches} \
    --truth-sensitivity-filter-level 99.7 \
    --mode SNP \
    -OVI \
    --output snp_recal.vcf.gz \
    --tmp-dir ${tmp}
  """

}