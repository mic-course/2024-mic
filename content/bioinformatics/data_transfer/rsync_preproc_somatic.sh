#!/bin/bash
#SBATCH --job-name=rsync-preproc-somatic
#SBATCH --account=mic-2024
#SBATCH --mail-user=lwr11@duke.edu
#SBATCH --mail-type=END,FAIL
#SBATCH --output=./logs/rsync-preproc-somatic-log-%j

# --- look into error
srun \
  --partition=chsi-high \
  --account=mic-2024 \
  --nodes=1 \
  --ntasks-per-node=1 \
  --cpus-per-task=20 \
  --pty bash -i

d1=/work/mic-2024-gatk/proc/
d2=/hpc/group/mic-2024/proc

rsync -av --progress ${d1} ${d2}
# sending incremental file list

# sent 202,014 bytes  received 1,817 bytes  16,306.48 bytes/sec
# total size is 13,120,119,532,722  speedup is 64,367,635.60