[One-based versus zero-based genomic coordinates](https://www.biostars.org/p/84686/)

[BED Format](https://useast.ensembl.org/info/website/upload/bed.html)

[GATK-style List Format](https://gatk.broadinstitute.org/hc/en-us/articles/360035531852-Intervals-and-interval-lists)


[BedToIntervalList](https://gatk.broadinstitute.org/hc/en-us/articles/360036883931-BedToIntervalList-Picard)


[LiftOverIntervalList](https://gatk.broadinstitute.org/hc/en-us/articles/360037224632-LiftOverIntervalList-Picard)

[Liftover chain](https://hgdownload.soe.ucsc.edu/goldenPath/hg38/liftOver/)
