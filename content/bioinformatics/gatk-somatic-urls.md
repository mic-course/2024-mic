[Mutect2](https://gatk.broadinstitute.org/hc/en-us/articles/21905083931035-Mutect2)


[GenomicsDBImport](https://gatk.broadinstitute.org/hc/en-us/articles/21905031525019-GenomicsDBImport)

[CreateSomaticPanelOfNormals](https://gatk.broadinstitute.org/hc/en-us/articles/21905137669019-CreateSomaticPanelOfNormals-BETA)

[LearnReadOrientationModel](https://gatk.broadinstitute.org/hc/en-us/articles/21905002317339-LearnReadOrientationModel)

[GetPileupSummaries](https://gatk.broadinstitute.org/hc/en-us/articles/21905080913947-GetPileupSummaries)

[CalculateContamination](https://gatk.broadinstitute.org/hc/en-us/articles/21905021795739-CalculateContamination)


[FilterMutectCalls](https://gatk.broadinstitute.org/hc/en-us/articles/21905009596187-FilterMutectCalls)
