# IGV Workshop
## Scavenger Hunt
1. Can you tell which samples were sequenced on HiSeq and which on NovaSeq? (Hint the HiSeq reads are shorter in this dataset)
2. Genes mentioned in the paper: BAP1, FBXW7, NF2, etc
3. Reads with large inserts
4. Reads with small inserts
5. Unpaired reads
6. Reads with sequencing error(s)
7. True variants
8. Areas with no reads
9. Find an insertion

## Load more BAMs
Load some more full size BAMs. You can find them in `/hpc/group/mic-2024//proc/stage3/ApplyBQSR`

# Gviz Workshop
## Read the Docs
Look over the documentation for Gviz to see examples of what you can do with it:
1. [Gviz User Guide](https://bioconductor.org/packages/release/bioc/vignettes/Gviz/inst/doc/Gviz.html)
2. [Gviz Reference Manual](https://bioconductor.org/packages/release/bioc/manuals/Gviz/man/Gviz.pdf)

## Try to make some new plots
1. One of the other genes mentioned in the paper
2. Add an annotation track (e.g. exons)
3. Simulate some data and add a data track
